/**
* ref https://bl.ocks.org/mbostock/1046712
* https://bl.ocks.org/mbostock/4062006
**/
import React from 'react';
import PropTypes from 'prop-types';
import * as d3 from 'd3';
import Chart from '../../core/Chart';

class ChordChart extends Chart {
  constructor(props) {
    super(props);
    this.init();
  }

  init() {
    this.outerRadius = 960 / 2;
    this.innerRadius = this.outerRadius - 130;
  }

  componentDidMount() {
    this.drawChordChart();
  }

  drawChordChart() {
    this.chordData = this.chord(this.props.config.data);
    
    this.chordChart = d3.select(this.svg)
      .append('g')
      .attr('transform', `translate(${this.size.width / 2}, ${this.size.height / 2})`)
      .datum(this.chordData);

    this.drawPaths();
    this.drawRibbons();
    this.drawGroupTicks();
  }

  drawPaths() {
    this.group = this.chordChart
      .append('g')
      .attr('class', 'groups')
      .selectAll('g')
      .data(this.chordData.groups)
      .enter()
      .append('g');

    this.group
      .append('path')
      .style('fill', (d) => {
        return this.color(d.index)
      })
      .style('stroke', (d) => {
        return d3.rgb(this.color(d.index)).darker();
      })
      .attr("d", this.arc);
  }

  drawRibbons() {
    this.chordChart.append("g")
      .attr("class", "ribbons")
      .selectAll("path")
      .data(this.chordData)
      .enter().append("path")
      .attr("d", this.ribbon)
      .style("fill", (d) => { 
        return this.color(d.target.index); 
      })
      .style("stroke", (d) => {
        return d3.rgb(this.color(d.target.index)).darker(); 
      });
  }

  drawGroupTicks() {
    this.groupTick = this.group.selectAll(".group-tick")
      .data((d) => { 
        return this.groupTicks(d, 1e3); 
      })
      .enter().append("g")
      .attr("class", "group-tick")
      .attr("transform", (d) => { 
        return `rotate(${d.angle * 180 / Math.PI - 90}) translate(${this.outerRadius},0)`; 
      });

    this.groupTick
      .append('line')
      .style('stroke', 'black')
      .attr('x2', 6);

    this.groupTick
      .filter((d) => { 
        return d.value % 5e3 === 0; 
      })
      .append("text")
      .attr("x", 8)
      .attr("dy", ".35em")
      .attr("transform", (d) => { 
        return d.angle > Math.PI ? "rotate(180) translate(-16)" : null; 
      })
      .style("text-anchor", (d) => { 
        return d.angle > Math.PI ? "end" : null; 
      })
      .text((d) => { 
        return this.formatValue(d.value); 
      });
  }
  
  // Returns an array of tick angles and values for a given group and step.
  groupTicks(d, step) {
    var k = (d.endAngle - d.startAngle) / d.value;
    
    return d3.range(0, d.value, step).map(function(value) {
      return {value: value, angle: value * k + d.startAngle};
    });
  }

  render() {
    return (
      <div className='chord-chart-container'>
        <svg
          width={this.size.width}
          height={this.size.height}
          ref={(c) => {
            this.svg = c;
          }}
        >
        </svg>
      </div>
    );
  }
}

ChordChart.propTypes = {
  config: PropTypes.object.isRequired
};

export default ChordChart
