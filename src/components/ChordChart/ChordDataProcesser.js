import _ from 'lodash';
import * as d3 from 'd3';

class ChordDataProcesser {
  static parse(data, target) {
    let newData = [];

    _.each(data, (v, k) => {
      _.each(v, (v1, k1) => {
        newData.push({source: k, target: k1, value: v1[target].sum});
      })
    });

    return newData;
  }
}

export default ChordDataProcesser;
