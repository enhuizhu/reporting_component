/**
* ref: https://bl.ocks.org/mbostock/2e73ec84221cb9773f4c
**/
import React from 'react';
import PropTypes from 'prop-types';
import * as d3 from "d3";
import ChartLegend from '../ChartLegend/ChartLegend';
import Chart from '../../core/Chart';
import _ from 'lodash';

class PartitionChart extends Chart {
  constructor(props) {
    super(props);
    this.state = {
      legendsData: [],
    }
    this.resetChart = this.resetChart.bind(this);
    this.prepareData();
    this.init();
  }

  componentDidMount() {
    this.addResizeEvents();
    this.resetChart();
    this.setState({
      legendsData: this.getLengedsData(),
    });    
  }

  componentDidUpdate(prevProps, prevState) {
    if (!_.isEqual(this.props.config.data, prevProps.config.data)) {
      this.prepareData();
      this.resetChart();
      this.setState({
        legendsData: this.getLengedsData(),
      });
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resetChart);
  }

  prepareData() {
    this.root = d3.hierarchy(this.props.config.data)
      .sum((d) => { 
        return d.size; 
      })
      .sort((a, b) => { 
        return b.value - a.value; 
      });
  }

  getLengedsData() {
    if (!this.root.children) {
      return [];
    }

    const target = 1;

    const data = this.root.children.filter((v, i) => {
      return v.depth === target;
    }).map((v) => {
      return {
        color: this.color(v.data.name),
        text: v.data.name,
      }
    });

    return data;
  }
  
  init() {
    this.color = d3.scaleOrdinal(d3.schemeCategory10);
  }

  addResizeEvents() {
    window.addEventListener('resize', this.resetChart);
  }

  resetChart() {
    this.size = this.getSvgSize();
    this.x = d3.scaleLinear().range([0, this.size.width]).domain([0, this.size.width]);
    this.y = d3.scaleLinear().range([0, this.size.height]).domain([0, this.size.height]);
    
    d3.select(this.svg)
      .attr('width', this.size.width)
      .attr('height', this.size.height);

    this.partition = d3.partition()
      .size([this.size.height, this.size.width])
      .padding(1)
      .round(true);

    this.partition(this.root);

    this.draw();
  }

  draw() {
    const nodes = d3.select(this.svg)
      .selectAll(".node")
      .data(this.root.descendants());
    
    const cell = nodes
      .enter().append("g")
      .merge(nodes)
        .attr("class", (d) => { 
          return "node" + (d.children ? " node--internal" : " node--leaf"); 
        })
        .attr("transform", (d) => { 
          return "translate(" + this.y(d.y0) + "," + this.x(d.x0) + ")";
        });

    cell.select('rect').remove();
    
    cell.append("rect")
      .attr("id", (d) => { 
        return "rect-" + d.data.name; 
      })
      .attr("width", (d) => { 
        return this.y(d.y1 - d.y0); 
      })
      .attr("height", (d) => { 
        return this.x(d.x1 - d.x0); 
      })
      // .filter((d) => { 
      //   return !d.children; 
      // })
        .style("fill", (d) => { 
          while (d.depth > 1) d = d.parent; 
          return this.color(d.data.name); 
        });

    cell.select('clipPath').remove();
    
    cell.append("clipPath")
      .attr("id", (d) => { 
        return "clip-" + d.data.name; 
      })
    .append("use")
      .attr("xlink:href", (d) => { 
        return "#rect-" + d.data.name + ""; 
      });

    cell.select('text').remove();
    
    cell.append("text")
      .attr("clip-path", (d) => { 
        return "url(#clip-" + d.data.name + ")"; 
      })
      .attr("x", 4)
    .selectAll("tspan")
      .data((d) => { 
        return [d.data.name, " " + this.formatValue(d.value)]; 
      })
    .enter().append("tspan")
      .attr("y", (d, i) => {
        return 13 * (i + 1);
      })
      .attr("x", 3)
      .text((d) => { 
        return d; 
      });

    cell.select('title').remove();

    cell.append("title")
      .text((d) => { 
        return d.data.name + "\n" + this.formatValue(d.value); 
      });

    nodes.exit().remove(); 
  }

  render() {
    return (
      <div className='partition-chart-container' ref={(c) => {
        this.container = c;
      }}>
        <svg ref={(c) => {
          this.svg = c;
        }}>
        </svg>

        <ChartLegend
          data={this.state.legendsData}
        />    
      </div>
    );
  }
}

export default PartitionChart
