import React from 'react';
import PropTypes from 'prop-types';
import Chart from '../../core/Chart';
import * as d3 from "d3";
import d3Tip from 'd3-tip';
import ChartLegend from '../ChartLegend/ChartLegend';

class PieChart extends Chart {  
  constructor(props) {
    super(props);
    this.init();
  }

  componentDidMount() {
    this.drawChart();
    d3.select(this.svg).call(this.tooltip);
  }
  
  componentDidUpdate(prevProps, prevState) {
    this.setTotalSize();
    this.drawChart();
  }
  
  componentWillUnmount() {
    window.removeEventListener('resize', this.drawChart);
  }

  getLegendData() {
    return this.props.config.data.map((v) => {
      return {
        text: v.name,
        color: this.color(v.name),
      }
    });  
  }

  init() {
    this.pie = d3.pie()
      .sort(null)
      .value(d => {
          return d.value;
      });

    this.padding = 10;
    this.setTotalSize();
    
    this.drawChart = this.drawChart.bind(this);
    this.getLabelText = this.getLabelText.bind(this);
    this.mouseover = this.mouseover.bind(this);
    this.mouseleave = this.mouseleave.bind(this);

    this.tooltip = d3Tip()
      .attr("class", "d3-tip")
      .offset([0, 0])
      .html(this.getLabelText);

    window.addEventListener('resize', this.drawChart);
  }

  mouseover(d) {
    this.tooltip.show(d);
  }

  mouseleave() {
    this.tooltip.hide();
  }

  setTotalSize() {
    this.totalSize = this.props.config.data.reduce((sum, current) => {
      return sum += current.value;
    }, 0);

    console.log('totalSize', this.totalSize);
  }

  getLabelText(d) {
    return `${d.data.name}: ${this.formatValue(d.value)} ${this.formatPercentage(d.value / this.totalSize)}`;
  }

  drawChart() {
    let that = this;
    this.size = this.getSvgSize();
    this.radius = Math.min(this.size.width, this.size.height) / 2;

    this.path = d3.arc()
      .outerRadius(this.radius - this.padding)
      .innerRadius(this.radius * 2 / 3);

    const arcTween = function(a) {
      const i = d3.interpolate(this._current, a);
      this._current = i(0);
      
      return function(t) {
          return that.path(i(t));
      };
    };

    d3.select(this.svg).select('g').remove();

    const arcs = d3.select(this.svg)
      .attr('width', this.size.width)
      .attr('height', this.size.height)
      .append('g')
      .on('mouseleave', this.mouseleave)
      .attr('transform', `translate(${this.size.width / 2}, ${this.size.height/2})`)
        .selectAll('.arc')
        .data(this.pie(this.props.config.data))

     arcs
       .enter()
          .append('path')
          .on('mouseover', this.mouseover)
          .attr('class', 'arc')
          .each(function(d) { 
              this._current = d; 
          })
       .merge(arcs)
          .transition()
          .duration(1000) 
            .attrTween("d", arcTween)
            .attr('fill', (d, i) => {
                return this.color(d.data.name);
            });

    arcs.exit().remove();
  }
  
  render() {
    return (
      <div className='pie-chart-container' ref={(c) => {
        this.container = c;
      }}>
        <svg ref={(c) => {
          this.svg = c;
        }}>
        </svg>

         <ChartLegend
          data={this.getLegendData()}
        />      
      </div>
    );
  }
}

PieChart.propTypes = {
  config: PropTypes.object.isRequired
};

export default PieChart
