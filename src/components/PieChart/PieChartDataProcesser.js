import _ from 'lodash';

class PieChartDataProcesser {
  static parse(data, target) {
    let newData = [];
    
    _.each(data, (v, k) => {
      newData.push({
        name: k,
        value: v[target].sum
      });
    });

    return newData;
  }
}

export default PieChartDataProcesser;
