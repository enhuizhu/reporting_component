import React from 'react';
import PropTypes from 'prop-types';
import * as d3 from "d3";
import sankey from './sankey';

class SankeyChart extends React.Component {  
  constructor(props) {
    super(props);
    this.init();
  }

  componentDidMount() {
    this.drawChart();
  }
  
  init() {
    this.margin = {
      top: 10,
      right: 10,
      bottom: 10,
      left: 10
    }

    this.size = this.props.config.size ? this.props.config.size : {width: 700, height: 300}
    this.formatNumber = d3.format(",.0f");
    this.color = d3.scaleOrdinal(d3.schemeCategory20);
    
    this.sankeySize = {
      width: this.size.width - this.margin.left - this.margin.right,
      height: this.size.height - this.margin.top - this.margin.bottom
    };

    this.sankey = sankey(this.sankeySize.width)
      .nodeWidth(5)
      .nodePadding(40)
      .size([this.sankeySize.width, this.sankeySize.height]);

    this.path = this.sankey.link();
    this.graph = {'nodes': [], 'links': []};

    this.setUpGraphData();
  }

  setUpGraphData() {
    this.props.config.data.forEach((d) => {
      this.graph.nodes.push({'name': d.source});
      this.graph.nodes.push({'name': d.target});
      this.graph.links.push({
        'source': d.source,
        'target': d.target,
        'value': +d.value
      });
    });

    const nestData = d3.nest()
       .key(function (d) { return d.name; })
       .map(this.graph.nodes);

    // return only the district / unique nodes
    this.graph.nodes = Object.keys(nestData).map(v => {return v.substr(1)});

    // loop through each link replacing the text with its index from node
    this.graph.links.forEach((d, i) => {
       this.graph.links[i].source = this.graph.nodes.indexOf(this.graph.links[i].source);
       this.graph.links[i].target = this.graph.nodes.indexOf(this.graph.links[i].target);
    });

    //now loop through each nodes to make nodes an array of objects
    // rather than an array of strings
    this.graph.nodes.forEach((d, i) => {
      this.graph.nodes[i] = { "name": d };
    });

    this.sankey
      .nodes(this.graph.nodes)
      .links(this.graph.links)
      .layout(32);
  }

  drawChart() {
    this.sankeyChart = d3.select(this.svg)
      .attr('width', this.size.width)
      .attr('height', this.size.height)
    .append('g')
      .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`)
    
    this.addLinks();
    this.addNodes();
  }

  addLinks() {
    let link = this.sankeyChart.append("g").selectAll(".link")
      .data(this.graph.links)
    .enter().append("path")
      .attr("class", "link")
      .attr("d", this.path)
      .style("stroke", (d, i) => {
        return '#000';
      })
      .style("stroke-width", (d) => { 
        return Math.max(1, d.dy); 
      })
      .sort((a, b) => { 
        return b.dy - a.dy; 
      });

    link.append("title")
      .text((d) => {
        return d.source.name + " → " + 
              d.target.name + "\n" + this.formatNumber(d.value); 
      });
  }

  addNodes() {
    let node = this.sankeyChart.append("g").selectAll(".node")
      .data(this.graph.nodes)
    .enter().append("g")
      .attr("class", "node")
      .attr("transform", function(d) { 
        return "translate(" + d.x + "," + d.y + ")"; 
      });  

    // add the rectangles for the nodes
    node.append("rect")
      .attr("height", function(d) { 
        return d.dy; 
      })
      .attr("width", this.sankey.nodeWidth())
      .style("fill", (d) => { 
        console.log(d);
        return d.color = this.color(d.name); 
      })
      .style("stroke", function(d) { 
        return d3.rgb(d.color).darker(2); 
      })
    .append("title")
      .text((d) => { 
        return d.name + "\n" + this.formatNumber(d.value); 
      });

    // add in the title for the nodes
    node.append("text")
      .attr("x", -6)
      .attr("y", (d) => { return d.dy / 2; })
      .attr("dy", ".35em")
      .attr("text-anchor", "end")
      .attr("transform", null)
      .text((d) => { return d.name; })
    .filter((d) => { return d.x < this.size.width / 2; })
      .attr("x", 6 + this.sankey.nodeWidth())
      .attr("text-anchor", "start");
  }

  render() {
    return (
      <div className='sankey-chart-container'>
        <svg ref={(c) =>{
          this.svg = c;
        }}></svg>
      </div>
    );
  }
}

export default SankeyChart
