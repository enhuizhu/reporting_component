/**
* ref http://bl.ocks.org/ganeshv/6a8e9ada3ab7f2d88022
**/

import React from 'react';
import PropTypes from 'prop-types';
import * as d3 from "d3";
import _ from 'lodash';
import ChartLegend from '../ChartLegend/ChartLegend';

class TreeMapChart extends React.Component {  
  constructor(props) {
    super(props);
    this.state = {
      showTop: false,
      legendData: [],
    };
    this.resetChart = this.resetChart.bind(this);
    this.init();
    this.rect = this.rect.bind(this);
    this.text2 = this.text2.bind(this);
    this.text = this.text.bind(this);
    this.transition = this.transition.bind(this);
    this.goTop = this.goTop.bind(this);
    this.legendMouseOver = this.legendMouseOver.bind(this);
    this.lengedMouseOut = this.lengedMouseOut.bind(this);
  }

  componentDidMount() {
    this.resetChart();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resetChart);
  }

  componentDidUpdate(prevProps, prevState) {
    if (!_.isEqual(this.props.config.data, prevProps.config.data)) {
      this.resetChart();
    }
  }

  init() {
    this.margin = {top: 0, right:0, bottom: 0, left: 0};
    this.format = d3.format(',d');
    this.color = d3.scaleOrdinal(d3.schemeCategory20c);
    this.transitioning = false;
    this.addResizeEvents();
  }

  goTop() {
    if (this.currentParent) {
      this.transition(this.currentParent);
    }
  }

  addResizeEvents() {
    window.addEventListener('resize', this.resetChart);
  }

  getSvgSize() {
    const height = 500;
    const containerWidth = this.getContainerWidth();

    return {
      width: containerWidth,
      height,
    }
  }

  getContainerWidth() {
    return this.container.getBoundingClientRect().width;
  }

  legendMouseOver(d, i) {
    // this.mouseover(d, false);
  }

  lengedMouseOut() {
    // this.mouseleave();
  }

  getLengedsData(data) {
    return data.map((v) => {
      return {
        color: this.color(v.data.name),
        text: v.data.name,
      }
    });
  }

  resetChart() {
    d3.select(this.chart).selectAll('.depth').remove();
    this.size = this.getSvgSize();
    this.x = d3.scaleLinear().range([0, this.size.width]).domain([0, this.size.width]);
    this.y = d3.scaleLinear().range([0, this.size.height]).domain([0, this.size.height]);
    this.prepareData();
    this.setChartSize();
    // this.initGrandparent();
    this.draw(this.root);
  }

  setChartSize() {
    d3.select(this.svg)
      .attr('width', this.size.width + this.margin.left + this.margin.right)
      .attr('height', this.size.height + this.margin.bottom + this.margin.top)
      .style('margin-left', -this.margin.left + 'px')
      .style('margin-right', -this.margin.right + 'px')

    this.treeMapchart = d3.select(this.chart)
      .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`)
      .style("shape-rendering", "crispEdges");
  }

  prepareData() {
    this.treemap = d3.treemap()
      .tile(d3.treemapResquarify)
      .size([this.size.width, this.size.height])
      .round(true)
      .paddingInner(1);
    
    this.root = d3.hierarchy(this.props.config.data)
      .eachBefore((d) => { 
        d.data.id = (d.parent ? d.parent.data.id + "." : "") + d.data.name; 
      })
      .sum((d) => { return d.size })
      .sort((a, b) => { return b.height - a.height || b.value - a.value; });


    this.root.x = this.root.y = 0;
    this.root.dx = this.size.width;
    this.root.dy = this.size.height;
    this.root.depth = 0;

    this.treemap(this.root);
  }

  transition(d) {
    if (this.transitioning || !d) return;
    
    this.transitioning = true;
    
    let t1 = this.g1.transition().duration(750);
    let g2 = this.draw(d);
    let t2 = g2.transition().duration(750);

    // Update the domain only after entering new elements.
    this.x.domain([d.x0, d.x1]);
    this.y.domain([d.y0, d.y1]);

    // Enable anti-aliasing during the transition.
    this.treeMapchart.style("shape-rendering", null);

    // Draw child nodes on top of parent nodes.
    this.treeMapchart.selectAll(".depth").sort(function(a, b) { return a.depth - b.depth; });

    // Fade-in entering text.
    g2.selectAll("text").style("fill-opacity", 0);

    // Transition to the new view.
    t1.selectAll(".ptext").call(this.text).style("fill-opacity", 0);
    t1.selectAll(".ctext").call(this.text2).style("fill-opacity", 0);
    t2.selectAll(".ptext").call(this.text).style("fill-opacity", 1);
    t2.selectAll(".ctext").call(this.text2).style("fill-opacity", 1);
    t1.selectAll("rect").call(this.rect);
    t2.selectAll("rect").call(this.rect);

    // Remove the old node when the transition is finished.
    t1.remove().on("end", () => {
      this.treeMapchart.style("shape-rendering", "crispEdges");
      this.transitioning = false;
    });
  }

  rect(rect) {
   rect.attr("x", (d) => { return this.x(d.x0); })
      .attr("y", (d) => { return this.y(d.y0); })
      .attr("width", (d) => { return this.x(d.x1) - this.x(d.x0); })
      .attr("height", (d) => { return this.y(d.y1) - this.y(d.y0); });
  }

  text2(text) {
    const that = this;
    
    text.attr("x", function(d) { 
      return that.x(d.x1) - this.getComputedTextLength() - 6; 
    })
      .attr("y", (d) => { return this.y(d.y1) - 6; })
      .style("opacity", function(d) { 
        return this.getComputedTextLength() < that.x(d.x1) - that.x(d.x0) ? 1 : 0; 
      });
  }

  text(text) {
    const that = this;
    
    text.selectAll("tspan")
      .attr("x", (d) => { return this.x(d.x0) + 6; });
    
    text.attr("x", (d) => { return this.x(d.x0) + 6; })
      .attr("y", (d) => { return this.y(d.y0) + 6; })
      .style("opacity", function(d) { 
        return this.getComputedTextLength() < that.x(d.x1) - that.x(d.x0) ? 1 : 0; 
      });
  }

  draw(d) {
    if (_.isEmpty(d.children)) {
      this.setState({
        showTop: !!this.currentParent,
        legendData: [],
      });
      return ;
    } 

    this.currentParent = d.parent;

    this.setState({
      showTop: !!this.currentParent,
      legendData: this.getLengedsData(d.children),
    });


    this.g1 = d3.select(this.chart).append("g")
      .datum(d)
      .attr("class", "depth");

    const g = this.g1
      .selectAll('g')
      .data(d.children)
      .enter()
      .append('g');

    const parents = g.filter((d) => {return d.children;})
    
    parents.classed('children', true)
      .on('click', this.transition);

    g.append('rect')
      .attr('class', 'parent')
      .attr('fill', (d) => {
        return this.color(d.data.name)
      })
      .call(this.rect);

    const t = g.append('text')
      .attr('class', 'ptext')
      .attr('dy', '.75em');

    t.append('tspan')
      .text((d) => {
        return d.data.name;
      });

    t.append('tspan')
      .attr('dy', '1.2em')
      .text((d) => {
        return '£' + this.format(d.value);
      });

    t.call(this.text);

    const children = g.selectAll('.child')
      .data((d) => {return d.children || [d];})
      .enter().append('g');
    children.append('rect')
      .attr('class', 'child')
      .style('opacity', '0.05')
      .style('stroke-width', '1px')
      .call(this.rect)
      .append('title')
      .text((d) => {
        return `${d.data.name}(£${this.format(d.value)})`;
      });

    return g;
  }

  render() {
    return (
      <div className='tree-map-container' ref={(c) => {
        this.container = c;
      }}>
        {
          this.state.showTop ?
            <div className="tree-back btn btn-default btn-sm" onClick={this.goTop}>
              <span className="fa fa-level-up"></span>
            </div>: null
        }
        
        <svg ref={(c) => {
          this.svg = c;
        }}>
          <g className='tree-map' ref={(c) => {
            this.chart = c;
          }}>
          </g>
        </svg>

        <ChartLegend
          data={this.state.legendData} 
          mouseOver={this.legendMouseOver}
          mouseOut={this.lengedMouseOut}
        />
      </div>
    );
  }
}

TreeMapChart.propTypes = {
  config: PropTypes.object.isRequired
};

export default TreeMapChart
