import React from 'react';
import PropTypes from 'prop-types';
import * as d3 from "d3";
import _ from 'lodash';
import Chart from '../../core/Chart';

class TableChart extends Chart {
  constructor(props) {
    super(props);
    this.setHeaderWidth = this.setHeaderWidth.bind(this);
    this.prepareData(this.props.config.data);
    this.addResizeEvents();
  }

  componentDidMount() {
    this.setHeaderWidth();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.setHeaderWidth);
  }

  componentWillUpdate(nextProps, nextState) {
    this.prepareData(nextProps.config.data);
  }

  componentDidUpdate(prevProps, prevState) {
    this.setHeaderWidth();
  }

  addResizeEvents() {
    window.addEventListener('resize', this.setHeaderWidth);
  }

  setHeaderWidth() {
    if (!this.tbody) {
      return ;
    }

    let tdWidths =[];
    
    if (this.tbody.children.length) {
      for(let i =0; i < this.tbody.children[0].children.length; i++) {
        let item = this.tbody.children[0].children[i];
        this.header.children[i].style.width = item.getBoundingClientRect().width + 'px';
      }
    }
  }

  prepareData(data) {
    this.root = d3.hierarchy(data)
      .sum((d) => { 
        return d.size; 
      })
      .sort((a, b) => {
        return b.value - a.value; 
      });

    this.treemap = d3.treemap()
      .tile(d3.treemapResquarify)
      .round(true)
      .paddingInner(1);

    this.treemap(this.root);
  }

  getTds(v) {
    let tds = [];

    const name = v.data.name;
    const percentage = v.value / this.root.value;
    const value = v.value;
    
    while(v.parent) {
      if (v.parent.depth !== 0) {
        tds.push(
          <td 
          key={_.uniqueId()}
          >
            {v.parent.data.name}
          </td>
        );
      }
      
      v = v.parent;
    }

    tds.reverse();

    tds.push(<td key={_.uniqueId()}>{name}</td>);

    for(let i = tds.length; i < this.root.height; i++) {
      tds.push(<td key={_.uniqueId()}></td>);
    }
    
    tds.push(<td key={_.uniqueId()}>{this.formatPercentage(percentage)}</td>);
    tds.push(<td key={_.uniqueId()}>{this.formatValue(value)}</td>);

    return tds;
  }

  getChildren(child) {
    let trs = [];
    
    if (!child.children && !child.parent.render) {
      child.parent.children.map((v, i) => {
        trs.push(
          <tr key={_.uniqueId()}>
            {this.getTds(v)}
          </tr>
        );
      });

      child.parent.render = true;
    } else if(child.children) {
      child.children.map((v) => {
        trs = trs.concat(this.getChildren(v));
      });
    }
    
    return trs;
  }

  render() {
    if (!this.root.children) {
      return <div> No Data</div>;
    }

    let trs = this.getChildren(this.root);

    return (
      <div className="table-chart-container">
        <table className='table-header'>
          <thead>
            <tr ref={(c) => {
              this.header = c;
            }}>
              {
                new Array(this.root.height + 2).fill('').map((v, i) => {
                  return <td key={i}>{this.props.config.header[i]}</td>;
                })
              }
            </tr>
          </thead>
        </table>
        <div className="table-wrapper">
          <table>
            <tbody ref={(c) => {
              this.tbody = c;
            }}>
              {trs}
            </tbody>
            <tfoot>
              <tr>
                <td colSpan={this.root.height + 2} className='total'>
                  Total: {this.formatValue(this.root.value)}
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    );
  }
}

TableChart.propTypes = {
  config: PropTypes.object.isRequired
};

export default TableChart
