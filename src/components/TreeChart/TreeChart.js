/**
* https://bl.ocks.org/mbostock/4063582
**/

import React from 'react';
import PropTypes from 'prop-types';
import * as d3 from "d3";
import ChartLegend from '../ChartLegend/ChartLegend';
import _ from 'lodash';

class TreeChart extends React.Component {  
  constructor(props) {
    super(props);
    this.state = {
      lengendData: [],
    };
    this.init();
  }

  componentDidMount() {
    this.resetChart();
  }

  componentDidUpdate(prevProps, prevState) {
    if (!_.isEqual(this.props.config.data, prevProps.config.data)) {
      this.resetChart();
    }
  }

  init() {
    this.fader = (color) => {
      return d3.interpolateRgb(color, "#fff")(0.2);
    };

    this.color = d3.scaleOrdinal(d3.schemeCategory20.map(this.fader));
    
    this.format = (value) => {
      return `£${d3.format(",d")(value)}`;
    };
    
    this.addResizeEvents();
  }

  addResizeEvents() {
    window.addEventListener('resize', () => {
      // d3.select(this.chart).selectAll('.depth').remove();
      this.resetChart();
    });
  }

  resetChart() {
    this.size = this.getSvgSize();
    this.x = d3.scaleLinear().range([0, this.size.width]).domain([0, this.size.width]);
    this.y = d3.scaleLinear().range([0, this.size.height]).domain([0, this.size.height]);
    
    d3.select(this.svg)
      .attr('width', this.size.width)
      .attr('height', this.size.height);

    this.prepareData();
    this.draw();    
  }

  prepareData() {    
    this.treemap = d3.treemap()
      .tile(d3.treemapResquarify)
      .size([this.size.width, this.size.height])
      .round(true)
      .paddingInner(1);

    this.root = d3.hierarchy(this.props.config.data)
      .eachBefore((d) => {        
        d.data.id = (d.parent && d.parent.depth !== 0 ? d.parent.data.id + "." : "") + d.data.name; 
        // d.data.id = (d.parent ? d.parent.data.id + "." : "") + d.data.name; 
      })
      .sum((d) => {
        return d.size;
      })
      .sort((a, b) => { 
        return b.height - a.height || b.value - a.value; 
      });

    this.treemap(this.root);

    let lengendData = this.root.children.map((v) => {
      return {
        color: this.color(v.data.id),
        text: v.data.name,
      }
    });

    this.setState({
      lengendData,
    });
  }

  draw() {
    const allGroups = d3.select(this.svg)
      .selectAll("g")
      .data(this.root.leaves());
    
    const cell = allGroups
      .enter().append("g")
      .merge(allGroups)
        .attr("transform", (d) => {
          return "translate(" + this.x(d.x0) + "," + this.y(d.y0) + ")";
        });

    cell.select('rect').remove();
    
    cell.append("rect")
      .attr("id", (d) => { 
        return d.data.id; 
      })
      .attr("width", (d) => { 
        return this.x(d.x1 - d.x0);
      })
      .attr("height", (d) => { 
        return this.y(d.y1 - d.y0);
      })
      .attr("fill", (d) => {
       return this.color(d.parent.data.id);
     });

    cell.select('clipPath').remove();
    
    cell.append("clipPath")
      .attr("id", (d) => { 
        return "clip-" + d.data.id;
      })
    .append("use")
      .attr("xlink:href", (d) => { 
        return "#" + d.data.id;
      });

    cell.select('text').remove();
    
    cell.append("text")
      .attr("clip-path", (d) => { 
        return "url(#clip-" + d.data.id + ")"; 
      })
    .selectAll("tspan")
      .data((d) => { 
        return d.data.name.split(/(?=[A-Z][^A-Z])/g)
          .concat([this.format(d.value)]); 
      })
    .enter().append("tspan")
      .attr("x", 4)
      .attr("y", (d, i) => { 
        return 13 + i * 10; 
      })
      .text((d) => { 
        return d; 
      });

    cell.select('title').remove();

    cell.append("title")
      .text((d) => { 
        return d.data.id + "\n" + this.format(d.value);
      });

    allGroups.exit().remove();
  }

  getSvgSize() {
    const height = 570;
    const containerWidth = this.getContainerWidth();

    return {
      width: containerWidth,
      height,
    }
  }

  getContainerWidth() {
    return this.container.getBoundingClientRect().width;
  }

  render() {
    return (
      <div className='tree-chart-container' ref={(c) => {
        this.container = c;
      }}>
        <svg ref={(c) => {
          this.svg = c;
        }}>
        </svg>

        <ChartLegend data={this.state.lengendData}/>
      </div>
    );
  }
}

TreeChart.propTypes = {
  config: PropTypes.object.isRequired
};

export default TreeChart
 