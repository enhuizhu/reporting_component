import React from 'react';
import * as d3 from "d3";


class Chart extends React.Component {
  constructor(props) {
    super(props);

    this.color = d3.scaleOrdinal(d3.schemeCategory20);
    this.format = d3.format(',d');
    this.formatPercentage = d3.format(".0%");
    this.formatValue = (value) => {
      return `£${this.format(value)}`;
    };
  }

  getContainerWidth() {
    return this.container.getBoundingClientRect().width;
  }

  getSvgSize(height = 570) {
    const containerWidth = this.getContainerWidth();

    return {
      width: containerWidth,
      height,
    }
  }
}

export default Chart;
