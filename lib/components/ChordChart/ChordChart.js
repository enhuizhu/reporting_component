'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _d = require('d3');

var d3 = _interopRequireWildcard(_d);

var _Chart2 = require('../../core/Chart');

var _Chart3 = _interopRequireDefault(_Chart2);

var _ChartLegend = require('../ChartLegend/ChartLegend');

var _ChartLegend2 = _interopRequireDefault(_ChartLegend);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               * ref https://bl.ocks.org/mbostock/1046712
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               * https://bl.ocks.org/mbostock/4062006
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               **/


var ChordChart = function (_Chart) {
  _inherits(ChordChart, _Chart);

  function ChordChart(props) {
    _classCallCheck(this, ChordChart);

    var _this = _possibleConstructorReturn(this, (ChordChart.__proto__ || Object.getPrototypeOf(ChordChart)).call(this, props));

    _this.state = {
      legendData: []
    };

    _this.init();
    return _this;
  }

  _createClass(ChordChart, [{
    key: 'init',
    value: function init() {
      this.color = d3.scaleOrdinal(d3.schemeCategory20c);
      this.chord = d3.chord().padAngle(0.05).sortSubgroups(d3.descending).sortChords(d3.descending);
      this.arc = d3.arc().innerRadius(this.innerRadius).outerRadius(this.outerRadius);
      this.ribbon = d3.ribbon().radius(this.innerRadius);

      this.mouseenter = this.mouseenter.bind(this);
      this.mouseleave = this.mouseleave.bind(this);
      this.legendMouseOver = this.legendMouseOver.bind(this);
      this.lengedMouseOut = this.lengedMouseOut.bind(this);
      this.drawChordChart = this.drawChordChart.bind(this);

      window.addEventListener('resize', this.drawChordChart);
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.drawChordChart();
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      window.removeEventListener('resize', this.drawChordChart);
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps, prevState) {
      if (!_lodash2.default.isEqual(this.props.config.data, prevProps.config.data)) {
        this.drawChordChart();
      }
    }
  }, {
    key: 'getLegendData',
    value: function getLegendData(data) {
      var _this2 = this;

      return data.map(function (v, index) {
        return Object.assign({}, v, {
          text: _this2.nameByIndex.get(v.index),
          color: _this2.color(v.index)
        });
      });
    }
  }, {
    key: 'getChrodData',
    value: function getChrodData(data) {
      var _this3 = this;

      var matrix = [];
      var n = 0;
      this.indexByName = d3.map();
      this.nameByIndex = d3.map();
      var indexByTarget = d3.map();
      var targetByIndex = d3.map();

      data.forEach(function (d, i) {
        if (typeof _this3.indexByName.get(d.source) === 'undefined') {
          _this3.indexByName.set(d.source, n);
          _this3.nameByIndex.set(n, d.source);
          n++;
        }

        if (typeof indexByTarget.get(d.target) === 'undefined') {
          indexByTarget.set(d.target, i);
          targetByIndex.set(i, d.target);
        }
      });

      // Construct a square matrix counting package imports.
      data.forEach(function (d) {
        var source = _this3.indexByName.get(d.source),
            row = matrix[source];

        if (!row) {
          row = matrix[source] = [];

          for (var i = 0; i < data.length; i++) {
            row[i] = 0;
          }
        }

        row[indexByTarget.get(d.target)] = d.value;
      });

      return matrix;
    }
  }, {
    key: 'drawChordChart',
    value: function drawChordChart() {
      this.size = this.getSvgSize(700);
      this.outerRadius = Math.min(this.size.width, this.size.height) * 0.5 - 40;
      this.innerRadius = this.outerRadius - 25;
      this.arc = d3.arc().innerRadius(this.innerRadius).outerRadius(this.outerRadius);
      this.ribbon = d3.ribbon().radius(this.innerRadius);

      var metrics = this.getChrodData(this.props.config.data);

      this.chordData = this.chord(metrics);

      this.setState({
        legendData: this.getLegendData(this.chordData.groups)
      });

      d3.select(this.svg).attr('width', this.size.width).attr('height', this.size.height).select('g').remove();

      this.chordChart = d3.select(this.svg).append('g').attr('transform', 'translate(' + this.size.width / 2 + ', ' + this.size.height / 2 + ')').on('mouseleave', this.mouseleave).datum(this.chordData);

      this.drawPaths();
      this.drawRibbons();
    }
  }, {
    key: 'legendMouseOver',
    value: function legendMouseOver(d, i) {
      this.mouseenter(d.index);
    }
  }, {
    key: 'lengedMouseOut',
    value: function lengedMouseOut() {
      this.mouseleave();
    }
  }, {
    key: 'mouseenter',
    value: function mouseenter(index) {
      this.chordChart.selectAll('.ribbon').selectAll('path:not(.source_' + index).style('opacity', 0);
    }
  }, {
    key: 'mouseleave',
    value: function mouseleave() {
      this.chordChart.selectAll('.ribbon').selectAll('path').style('opacity', 1);
    }
  }, {
    key: 'drawPaths',
    value: function drawPaths() {
      var _this4 = this;

      this.group = this.chordChart.append('g').attr('class', 'groups').selectAll('g').data(this.chordData.groups).enter().append('g');

      this.group.append('path').style('fill', function (d) {
        return _this4.color(d.index);
      }).style('stroke', function (d) {
        return d3.rgb(_this4.color(d.index)).darker();
      }).attr("d", this.arc).attr("id", function (d) {
        return 'path-' + d.index;
      }).on('mouseenter', function (d) {
        _this4.mouseleave();
        _this4.mouseenter(d.index);
      }).each(function (d) {
        d.width = this.getBoundingClientRect().width;
      });

      this.group.append('text').attr('x', function (d) {
        return 6;
      }).attr('dy', 15).append('textPath').attr('xlink:href', function (d) {
        return '#path-' + d.index;
      }).text(function (d) {
        return _this4.nameByIndex.get(d.index);
      }).each(function (d) {
        d.textWidth = this.getBoundingClientRect().width;
      }).classed('hide', function (d) {
        return d.textWidth > d.width - 10;
      });

      this.group.append('text').each(function (d) {
        d.angle = (d.startAngle + d.endAngle) / 2;
      }).attr('x', function (d) {
        return d.angle > Math.PI ? -5 : 5;
      }).attr("dy", ".35em").attr("transform", function (d) {
        return "rotate(" + (d.angle * 180 / Math.PI - 90) + ")" + "translate(" + (_this4.innerRadius + 26) + ")" + (d.angle > Math.PI ? "rotate(180)" : "");
      }).style("text-anchor", function (d) {
        return d.angle > Math.PI ? "end" : null;
      }).text(function (d) {
        // return d.angle;
        return _this4.formatValue(d.value);
        return _this4.nameByIndex.get(d.index);
      });
    }
  }, {
    key: 'drawRibbons',
    value: function drawRibbons() {
      var _this5 = this;

      this.chordChart.append("g").attr("class", 'ribbon').selectAll("path").data(this.chordData).enter().append("path").attr("d", this.ribbon).style("fill", function (d) {
        return _this5.color(d.target.index);
      }).style("stroke", function (d) {
        return d3.rgb(_this5.color(d.target.index)).darker();
      }).attr('class', function (d) {
        return 'source_' + d.source.index;
      }).on('mouseenter', function (d) {
        _this5.mouseenter(d.source.index);
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _this6 = this;

      return _react2.default.createElement(
        'div',
        { className: 'chord-chart-container', ref: function ref(c) {
            _this6.container = c;
          } },
        _react2.default.createElement('svg', {
          ref: function ref(c) {
            _this6.svg = c;
          }
        }),
        _react2.default.createElement(_ChartLegend2.default, {
          data: this.state.legendData,
          mouseOver: this.legendMouseOver,
          mouseOut: this.lengedMouseOut
        })
      );
    }
  }]);

  return ChordChart;
}(_Chart3.default);

ChordChart.propTypes = {
  config: _propTypes2.default.object.isRequired
};

exports.default = ChordChart;