'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _d = require('d3');

var d3 = _interopRequireWildcard(_d);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _ChartLegend = require('../ChartLegend/ChartLegend');

var _ChartLegend2 = _interopRequireDefault(_ChartLegend);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               * ref http://bl.ocks.org/ganeshv/6a8e9ada3ab7f2d88022
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               **/

var TreeMapChart = function (_React$Component) {
  _inherits(TreeMapChart, _React$Component);

  function TreeMapChart(props) {
    _classCallCheck(this, TreeMapChart);

    var _this = _possibleConstructorReturn(this, (TreeMapChart.__proto__ || Object.getPrototypeOf(TreeMapChart)).call(this, props));

    _this.state = {
      showTop: false,
      legendData: []
    };
    _this.resetChart = _this.resetChart.bind(_this);
    _this.init();
    _this.rect = _this.rect.bind(_this);
    _this.text2 = _this.text2.bind(_this);
    _this.text = _this.text.bind(_this);
    _this.transition = _this.transition.bind(_this);
    _this.goTop = _this.goTop.bind(_this);
    _this.legendMouseOver = _this.legendMouseOver.bind(_this);
    _this.lengedMouseOut = _this.lengedMouseOut.bind(_this);
    return _this;
  }

  _createClass(TreeMapChart, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.resetChart();
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      window.removeEventListener('resize', this.resetChart);
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps, prevState) {
      if (!_lodash2.default.isEqual(this.props.config.data, prevProps.config.data)) {
        this.resetChart();
      }
    }
  }, {
    key: 'init',
    value: function init() {
      this.margin = { top: 0, right: 0, bottom: 0, left: 0 };
      this.format = d3.format(',d');
      this.color = d3.scaleOrdinal(d3.schemeCategory20c);
      this.transitioning = false;
      this.addResizeEvents();
    }
  }, {
    key: 'goTop',
    value: function goTop() {
      if (this.currentParent) {
        this.transition(this.currentParent);
      }
    }
  }, {
    key: 'addResizeEvents',
    value: function addResizeEvents() {
      window.addEventListener('resize', this.resetChart);
    }
  }, {
    key: 'getSvgSize',
    value: function getSvgSize() {
      var height = 500;
      var containerWidth = this.getContainerWidth();

      return {
        width: containerWidth,
        height: height
      };
    }
  }, {
    key: 'getContainerWidth',
    value: function getContainerWidth() {
      return this.container.getBoundingClientRect().width;
    }
  }, {
    key: 'legendMouseOver',
    value: function legendMouseOver(d, i) {
      // this.mouseover(d, false);
    }
  }, {
    key: 'lengedMouseOut',
    value: function lengedMouseOut() {
      // this.mouseleave();
    }
  }, {
    key: 'getLengedsData',
    value: function getLengedsData(data) {
      var _this2 = this;

      return data.map(function (v) {
        return {
          color: _this2.color(v.data.name),
          text: v.data.name
        };
      });
    }
  }, {
    key: 'resetChart',
    value: function resetChart() {
      d3.select(this.chart).selectAll('.depth').remove();
      this.size = this.getSvgSize();
      this.x = d3.scaleLinear().range([0, this.size.width]).domain([0, this.size.width]);
      this.y = d3.scaleLinear().range([0, this.size.height]).domain([0, this.size.height]);
      this.prepareData();
      this.setChartSize();
      // this.initGrandparent();
      this.draw(this.root);
    }
  }, {
    key: 'setChartSize',
    value: function setChartSize() {
      d3.select(this.svg).attr('width', this.size.width + this.margin.left + this.margin.right).attr('height', this.size.height + this.margin.bottom + this.margin.top).style('margin-left', -this.margin.left + 'px').style('margin-right', -this.margin.right + 'px');

      this.treeMapchart = d3.select(this.chart).attr('transform', 'translate(' + this.margin.left + ', ' + this.margin.top + ')').style("shape-rendering", "crispEdges");
    }
  }, {
    key: 'prepareData',
    value: function prepareData() {
      this.treemap = d3.treemap().tile(d3.treemapResquarify).size([this.size.width, this.size.height]).round(true).paddingInner(1);

      this.root = d3.hierarchy(this.props.config.data).eachBefore(function (d) {
        d.data.id = (d.parent ? d.parent.data.id + "." : "") + d.data.name;
      }).sum(function (d) {
        return d.size;
      }).sort(function (a, b) {
        return b.height - a.height || b.value - a.value;
      });

      this.root.x = this.root.y = 0;
      this.root.dx = this.size.width;
      this.root.dy = this.size.height;
      this.root.depth = 0;

      this.treemap(this.root);
    }
  }, {
    key: 'transition',
    value: function transition(d) {
      var _this3 = this;

      if (this.transitioning || !d) return;

      this.transitioning = true;

      var t1 = this.g1.transition().duration(750);
      var g2 = this.draw(d);
      var t2 = g2.transition().duration(750);

      // Update the domain only after entering new elements.
      this.x.domain([d.x0, d.x1]);
      this.y.domain([d.y0, d.y1]);

      // Enable anti-aliasing during the transition.
      this.treeMapchart.style("shape-rendering", null);

      // Draw child nodes on top of parent nodes.
      this.treeMapchart.selectAll(".depth").sort(function (a, b) {
        return a.depth - b.depth;
      });

      // Fade-in entering text.
      g2.selectAll("text").style("fill-opacity", 0);

      // Transition to the new view.
      t1.selectAll(".ptext").call(this.text).style("fill-opacity", 0);
      t1.selectAll(".ctext").call(this.text2).style("fill-opacity", 0);
      t2.selectAll(".ptext").call(this.text).style("fill-opacity", 1);
      t2.selectAll(".ctext").call(this.text2).style("fill-opacity", 1);
      t1.selectAll("rect").call(this.rect);
      t2.selectAll("rect").call(this.rect);

      // Remove the old node when the transition is finished.
      t1.remove().on("end", function () {
        _this3.treeMapchart.style("shape-rendering", "crispEdges");
        _this3.transitioning = false;
      });
    }
  }, {
    key: 'rect',
    value: function rect(_rect) {
      var _this4 = this;

      _rect.attr("x", function (d) {
        return _this4.x(d.x0);
      }).attr("y", function (d) {
        return _this4.y(d.y0);
      }).attr("width", function (d) {
        return _this4.x(d.x1) - _this4.x(d.x0);
      }).attr("height", function (d) {
        return _this4.y(d.y1) - _this4.y(d.y0);
      });
    }
  }, {
    key: 'text2',
    value: function text2(text) {
      var _this5 = this;

      var that = this;

      text.attr("x", function (d) {
        return that.x(d.x1) - this.getComputedTextLength() - 6;
      }).attr("y", function (d) {
        return _this5.y(d.y1) - 6;
      }).style("opacity", function (d) {
        return this.getComputedTextLength() < that.x(d.x1) - that.x(d.x0) ? 1 : 0;
      });
    }
  }, {
    key: 'text',
    value: function text(_text) {
      var _this6 = this;

      var that = this;

      _text.selectAll("tspan").attr("x", function (d) {
        return _this6.x(d.x0) + 6;
      });

      _text.attr("x", function (d) {
        return _this6.x(d.x0) + 6;
      }).attr("y", function (d) {
        return _this6.y(d.y0) + 6;
      }).style("opacity", function (d) {
        return this.getComputedTextLength() < that.x(d.x1) - that.x(d.x0) ? 1 : 0;
      });
    }
  }, {
    key: 'draw',
    value: function draw(d) {
      var _this7 = this;

      if (_lodash2.default.isEmpty(d.children)) {
        this.setState({
          showTop: !!this.currentParent,
          legendData: []
        });
        return;
      }

      this.currentParent = d.parent;

      this.setState({
        showTop: !!this.currentParent,
        legendData: this.getLengedsData(d.children)
      });

      this.g1 = d3.select(this.chart).append("g").datum(d).attr("class", "depth");

      var g = this.g1.selectAll('g').data(d.children).enter().append('g');

      var parents = g.filter(function (d) {
        return d.children;
      });

      parents.classed('children', true).on('click', this.transition);

      g.append('rect').attr('class', 'parent').attr('fill', function (d) {
        return _this7.color(d.data.name);
      }).call(this.rect);

      var t = g.append('text').attr('class', 'ptext').attr('dy', '.75em');

      t.append('tspan').text(function (d) {
        return d.data.name;
      });

      t.append('tspan').attr('dy', '1.2em').text(function (d) {
        return '£' + _this7.format(d.value);
      });

      t.call(this.text);

      var children = g.selectAll('.child').data(function (d) {
        return d.children || [d];
      }).enter().append('g');
      children.append('rect').attr('class', 'child').style('opacity', '0.05').style('stroke-width', '1px').call(this.rect).append('title').text(function (d) {
        return d.data.name + '(\xA3' + _this7.format(d.value) + ')';
      });

      return g;
    }
  }, {
    key: 'render',
    value: function render() {
      var _this8 = this;

      return _react2.default.createElement(
        'div',
        { className: 'tree-map-container', ref: function ref(c) {
            _this8.container = c;
          } },
        this.state.showTop ? _react2.default.createElement(
          'div',
          { className: 'tree-back btn btn-default btn-sm', onClick: this.goTop },
          _react2.default.createElement('span', { className: 'fa fa-level-up' })
        ) : null,
        _react2.default.createElement(
          'svg',
          { ref: function ref(c) {
              _this8.svg = c;
            } },
          _react2.default.createElement('g', { className: 'tree-map', ref: function ref(c) {
              _this8.chart = c;
            } })
        ),
        _react2.default.createElement(_ChartLegend2.default, {
          data: this.state.legendData,
          mouseOver: this.legendMouseOver,
          mouseOut: this.lengedMouseOut
        })
      );
    }
  }]);

  return TreeMapChart;
}(_react2.default.Component);

TreeMapChart.propTypes = {
  config: _propTypes2.default.object.isRequired
};

exports.default = TreeMapChart;