'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var SunburstDataProcesser = function () {
  function SunburstDataProcesser() {
    _classCallCheck(this, SunburstDataProcesser);
  }

  _createClass(SunburstDataProcesser, null, [{
    key: 'parse',
    value: function parse(data, target) {
      var obj = {
        name: 'Total',
        children: this.getChildren(data, target)
      };

      return obj;
    }
  }, {
    key: 'getChildren',
    value: function getChildren(v, target) {
      var _this = this;

      var children = [];

      _lodash2.default.each(v, function (cv, ck) {
        if (Object.keys(cv)[0] === target) {
          children.push({
            name: ck,
            size: cv[target].sum
          });
        } else {
          children.push({
            name: ck,
            children: _this.getChildren(cv, target)
          });
        }
      });

      return children;
    }
  }]);

  return SunburstDataProcesser;
}();

exports.default = SunburstDataProcesser;