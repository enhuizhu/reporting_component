'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _d = require('d3');

var d3 = _interopRequireWildcard(_d);

var _d3Tip = require('d3-tip');

var _d3Tip2 = _interopRequireDefault(_d3Tip);

var _ChartLegend = require('../ChartLegend/ChartLegend');

var _ChartLegend2 = _interopRequireDefault(_ChartLegend);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               * ref: https://bl.ocks.org/kerryrodden/7090426
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               **/

var SunburstChart = function (_React$Component) {
  _inherits(SunburstChart, _React$Component);

  function SunburstChart(props) {
    _classCallCheck(this, SunburstChart);

    var _this = _possibleConstructorReturn(this, (SunburstChart.__proto__ || Object.getPrototypeOf(SunburstChart)).call(this, props));

    _this.resetChartSize = _this.resetChartSize.bind(_this);
    _this.init();
    _this.prepareSunburstData(_this.props.config.data);
    return _this;
  }

  _createClass(SunburstChart, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.resetChartSize();
      d3.select(this.svg).call(this.tooltip);
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      window.removeEventListener('resize', this.resetChartSize);
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      this.prepareSunburstData(nextProps.config.data);
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps, prevState) {
      this.resetChartSize();
    }
  }, {
    key: 'init',
    value: function init() {
      var _this2 = this;

      this.svgMaxSize = { width: 450, height: 450 };
      this.color = d3.scaleOrdinal(d3.schemeCategory20c);
      this.partition = d3.partition();
      this.format = d3.format(",.2%");
      this.formatCurrency = d3.format(",.2f");
      this.x = d3.scaleLinear().range([0, 2 * Math.PI]);

      this.startAngle = function (d) {
        return Math.max(0, Math.min(2 * Math.PI, _this2.x(d.x0)));
      };

      this.endAngle = function (d) {
        return Math.max(0, Math.min(2 * Math.PI, _this2.x(d.x1)));
      };

      this.getColor = function (d) {
        return _this2.color((d.children ? d.data : d.parent.data).name);
      };

      this.innerRadius = function (d) {
        return Math.max(0, _this2.y(d.y0));
      };

      this.outerRadius = function (d) {
        return Math.max(0, _this2.y(d.y1));
      };

      this.midAngle = function (d) {
        var startAngle = _this2.startAngle(d);
        var endAngle = _this2.endAngle(d);
        return startAngle + (endAngle - startAngle) / 2;
      };

      this.arc = d3.arc().startAngle(this.startAngle).endAngle(this.endAngle).innerRadius(this.innerRadius).outerRadius(this.outerRadius);

      this.tooltip = (0, _d3Tip2.default)().attr("class", "d3-tip").offset([0, 0]).html(this.getLabelText.bind(this));

      this.addResizeEvents();
    }
  }, {
    key: 'getLabelText',
    value: function getLabelText(d) {
      return d.data.name + ': \xA3' + this.formatCurrency(d.value) + ' ' + this.format(d.value / this.totalSize);
    }
  }, {
    key: 'shouldShow',
    value: function shouldShow(d) {
      var perc = 100;

      if (d.parent && d.parent.value > 0 && d.value > 0) {
        perc = d.value / d.parent.value * 100;
      }

      return d.depth === 1 && perc > 3 && this.containerWidth > 600;
    }
  }, {
    key: 'addResizeEvents',
    value: function addResizeEvents() {
      window.addEventListener('resize', this.resetChartSize);
    }
  }, {
    key: 'getSvgSize',
    value: function getSvgSize() {
      var containerWidth = this.getContainerWidth();

      if (containerWidth >= this.svgMaxSize.width) {
        return this.svgMaxSize;
      }

      return {
        width: containerWidth,
        height: containerWidth
      };
    }
  }, {
    key: 'resetChartSize',
    value: function resetChartSize() {
      if (_lodash2.default.isEmpty(this.props.config.data.children)) {
        d3.select('.paths').selectAll('*').remove();
        d3.select('.labels').selectAll('*').remove();
        d3.select('.lines').selectAll('*').remove();
        this.message.innerHTML = 'No Data';
        return;
      } else {
        this.message.innerHTML = '';
      }

      this.size = this.getSvgSize();
      this.radius = Math.min(this.size.width, this.size.height) / 2;
      this.containerWidth = this.getContainerWidth();
      this.y = d3.scaleSqrt().range([0, this.radius]);

      this.sunburst = d3.select(this.svg).attr('width', this.size.width).attr('height', this.size.height).select('g.sunburst').attr('transform', 'translate(' + this.size.width / 2 + ', ' + this.size.height * .52 + ')');

      this.drawSunbust();
    }
  }, {
    key: 'prepareSunburstData',
    value: function prepareSunburstData(data) {
      this.root = d3.hierarchy(data).sum(function (d) {
        return d.size;
      }).sort(function (a, b) {
        return b.value - a.value;
      });

      this.nodes = this.partition(this.root).descendants().filter(function (d) {
        return d.x1 - d.x0 > 0.005; // 0.005 radians = 0.29 degrees
      });
    }
  }, {
    key: 'getLengedsData',
    value: function getLengedsData() {
      var _this3 = this;

      var target = this.nodes[0].height > 1 ? 2 : 1;

      var data = this.nodes.filter(function (v, i) {
        v.id = i;
        return v.depth === target;
      }).map(function (v) {
        return Object.assign(v, {
          color: _this3.getColor(v),
          text: v.data.name
        });
      });

      return data;
    }
  }, {
    key: 'legendMouseOver',
    value: function legendMouseOver(d, i) {
      this.mouseover(d, false);
    }
  }, {
    key: 'lengedMouseOut',
    value: function lengedMouseOut() {
      this.mouseleave();
    }
  }, {
    key: 'getContainerWidth',
    value: function getContainerWidth() {
      return this.container.getBoundingClientRect().width;
    }
  }, {
    key: 'drawSunbust',
    value: function drawSunbust() {
      this.totalSize = this.root.value;
      this.addPaths(this.nodes);
      this.addLabels(this.nodes);
      this.addLines(this.nodes);
      this.sunburst.on('mouseleave', this.mouseleave.bind(this));
    }
  }, {
    key: 'addLines',
    value: function addLines(nodes) {
      var _this4 = this;

      var lines = this.sunburst.select('.lines').selectAll('polyline').data(nodes);

      lines.enter().append('polyline').style('stroke', this.getColor).style('stroke-width', "0.5px").style('fill', 'none').merge(lines).attr('points', function (d) {
        return _this4.polyLinePoints(d).points;
      }).style('display', function (d) {
        return _this4.shouldShow(d) ? 'block' : 'none';
      });

      lines.exit().remove();
    }
  }, {
    key: 'addPaths',
    value: function addPaths(nodes) {
      var _this5 = this;

      var paths = this.sunburst.select("g.paths").selectAll('path').data(nodes);

      var path = paths.enter().append('path').style("stroke", "#fff").style("fill-rule", "evenodd").on('mouseover', this.mouseover.bind(this)).on('click', function (d) {
        d3.event.stopPropagation();
        _this5.click(d);
      }).merge(paths).attr("display", function (d) {
        return d.depth ? null : "none";
      }) // hide inner ring
      .style("fill", this.getColor).attr("d", this.arc);

      paths.exit().remove();

      d3.select(this.svg).on('click', function () {
        _this5.click(nodes[0]);
      });
    }
  }, {
    key: 'addLabels',
    value: function addLabels(nodes) {
      var _this6 = this;

      var texts = this.sunburst.select('.labels').selectAll('text').data(nodes);

      texts.enter().append('text').attr('dy', '-.35em').merge(texts).style('text-anchor', function (d) {
        return _this6.midAngle(d) > Math.PI ? 'start' : 'end';
      }).style('display', function (d) {
        return _this6.shouldShow(d) ? 'block' : 'none';
      }).attr('transform', function (d) {
        var pos = _this6.polyLinePoints(d).end;
        return 'translate(' + pos + ')';
      }).text(this.getLabelText.bind(this));

      texts.exit().remove();
    }
  }, {
    key: 'polyLinePoints',
    value: function polyLinePoints(d) {
      var centroid = this.arc.centroid(d);
      var inflection = [0, 0];
      var end = [0, 0];
      var distToChart = 20;
      var xDist = this.radius + distToChart;
      var lineWidth = this.containerWidth / 2 - this.radius - distToChart;

      if (centroid[0] > 0) {
        // right of center
        inflection[0] = xDist;
        end[0] = inflection[0] + lineWidth;
      } else {
        // left of center
        inflection[0] = -xDist;
        end[0] = inflection[0] - lineWidth;
      }

      if (centroid[0] < 0 && centroid[1] > 0 || centroid[0] > 0 && centroid[1] < 0) {
        inflection[1] = centroid[1] + (inflection[0] - centroid[0]) * Math.tan(-1 / 12 * Math.PI);
      } else {
        inflection[1] = centroid[1] + (inflection[0] - centroid[0]) * Math.tan(1 / 12 * Math.PI);
      }

      end[1] = inflection[1];

      return {
        points: centroid[0] + "," + centroid[1] + " " + inflection[0] + "," + inflection[1] + " " + end[0] + "," + end[1],
        end: end
      };
    }
  }, {
    key: 'click',
    value: function click(d) {
      var _this7 = this;

      this.sunburst.transition().duration(750).tween("scale", function () {
        var xd = d3.interpolate(_this7.x.domain(), [d.x0, d.x1]),
            yd = d3.interpolate(_this7.y.domain(), [d.y0, 1]),
            yr = d3.interpolate(_this7.y.range(), [d.y0 ? 20 : 0, _this7.radius]);
        return function (t) {
          _this7.x.domain(xd(t));
          _this7.y.domain(yd(t)).range(yr(t));
        };
      }).selectAll("path").attrTween("d", function (d) {
        return function () {
          return _this7.arc(d);
        };
      });

      this.sunburst.selectAll('.labels, .lines').style('display', function () {
        return d.depth === 0 ? 'block' : 'none';
      });
    }
  }, {
    key: 'mouseleave',
    value: function mouseleave() {
      this.sunburst.selectAll('path').style('opacity', 1);

      this.tooltip.hide();
    }

    //Given a node in a partition layout, return an array of all of its ancestor
    // nodes, highest first, but excluding the root.

  }, {
    key: 'getAncestors',
    value: function getAncestors(node) {
      var path = [];
      var current = node;
      while (current.parent) {
        path.unshift(current);
        current = current.parent;
      }
      return path;
    }
  }, {
    key: 'mouseover',
    value: function mouseover(d) {
      var showTip = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

      var percentage = (100 * d.value / this.totalSize).toPrecision(3);
      var percentageString = percentage + "%";
      var sequenceArray = this.getAncestors(d);

      if (showTip) {
        this.tooltip.show(d);
      }

      // Fade all the segments.
      this.sunburst.selectAll("path").style("opacity", 0.3);

      // Then highlight only those that are an ancestor of the current segment.
      this.sunburst.selectAll("path").filter(function (node) {
        return sequenceArray.indexOf(node) >= 0;
      }).style("opacity", 1);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this8 = this;

      return _react2.default.createElement(
        'div',
        { className: 'sunburst-chart-container', ref: function ref(c) {
            _this8.container = c;
          } },
        _react2.default.createElement(
          'svg',
          {
            ref: function ref(c) {
              _this8.svg = c;
            } },
          _react2.default.createElement(
            'g',
            { className: 'sunburst' },
            _react2.default.createElement('g', { className: 'paths' }),
            _react2.default.createElement('g', { className: 'labels' }),
            _react2.default.createElement('g', { className: 'lines' })
          )
        ),
        _react2.default.createElement(_ChartLegend2.default, {
          data: this.getLengedsData(),
          mouseOver: this.legendMouseOver.bind(this),
          mouseOut: this.lengedMouseOut.bind(this)
        }),
        _react2.default.createElement('div', { className: 'error', ref: function ref(c) {
            _this8.message = c;
          } })
      );
    }
  }]);

  return SunburstChart;
}(_react2.default.Component);

SunburstChart.propTypes = {
  config: _propTypes2.default.object.isRequired
};

exports.default = SunburstChart;