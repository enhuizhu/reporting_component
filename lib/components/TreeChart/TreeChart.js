'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _d = require('d3');

var d3 = _interopRequireWildcard(_d);

var _ChartLegend = require('../ChartLegend/ChartLegend');

var _ChartLegend2 = _interopRequireDefault(_ChartLegend);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               * https://bl.ocks.org/mbostock/4063582
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               **/

var TreeChart = function (_React$Component) {
  _inherits(TreeChart, _React$Component);

  function TreeChart(props) {
    _classCallCheck(this, TreeChart);

    var _this = _possibleConstructorReturn(this, (TreeChart.__proto__ || Object.getPrototypeOf(TreeChart)).call(this, props));

    _this.state = {
      lengendData: []
    };
    _this.init();
    return _this;
  }

  _createClass(TreeChart, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.resetChart();
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps, prevState) {
      if (!_lodash2.default.isEqual(this.props.config.data, prevProps.config.data)) {
        this.resetChart();
      }
    }
  }, {
    key: 'init',
    value: function init() {
      this.fader = function (color) {
        return d3.interpolateRgb(color, "#fff")(0.2);
      };

      this.color = d3.scaleOrdinal(d3.schemeCategory20.map(this.fader));

      this.format = function (value) {
        return '\xA3' + d3.format(",d")(value);
      };

      this.addResizeEvents();
    }
  }, {
    key: 'addResizeEvents',
    value: function addResizeEvents() {
      var _this2 = this;

      window.addEventListener('resize', function () {
        // d3.select(this.chart).selectAll('.depth').remove();
        _this2.resetChart();
      });
    }
  }, {
    key: 'resetChart',
    value: function resetChart() {
      this.size = this.getSvgSize();
      this.x = d3.scaleLinear().range([0, this.size.width]).domain([0, this.size.width]);
      this.y = d3.scaleLinear().range([0, this.size.height]).domain([0, this.size.height]);

      d3.select(this.svg).attr('width', this.size.width).attr('height', this.size.height);

      this.prepareData();
      this.draw();
    }
  }, {
    key: 'prepareData',
    value: function prepareData() {
      var _this3 = this;

      this.treemap = d3.treemap().tile(d3.treemapResquarify).size([this.size.width, this.size.height]).round(true).paddingInner(1);

      this.root = d3.hierarchy(this.props.config.data).eachBefore(function (d) {
        d.data.id = (d.parent && d.parent.depth !== 0 ? d.parent.data.id + "." : "") + d.data.name;
        // d.data.id = (d.parent ? d.parent.data.id + "." : "") + d.data.name; 
      }).sum(function (d) {
        return d.size;
      }).sort(function (a, b) {
        return b.height - a.height || b.value - a.value;
      });

      this.treemap(this.root);

      var lengendData = this.root.children.map(function (v) {
        return {
          color: _this3.color(v.data.id),
          text: v.data.name
        };
      });

      this.setState({
        lengendData: lengendData
      });
    }
  }, {
    key: 'draw',
    value: function draw() {
      var _this4 = this;

      var allGroups = d3.select(this.svg).selectAll("g").data(this.root.leaves());

      var cell = allGroups.enter().append("g").merge(allGroups).attr("transform", function (d) {
        return "translate(" + _this4.x(d.x0) + "," + _this4.y(d.y0) + ")";
      });

      cell.select('rect').remove();

      cell.append("rect").attr("id", function (d) {
        return d.data.id;
      }).attr("width", function (d) {
        return _this4.x(d.x1 - d.x0);
      }).attr("height", function (d) {
        return _this4.y(d.y1 - d.y0);
      }).attr("fill", function (d) {
        return _this4.color(d.parent.data.id);
      });

      cell.select('clipPath').remove();

      cell.append("clipPath").attr("id", function (d) {
        return "clip-" + d.data.id;
      }).append("use").attr("xlink:href", function (d) {
        return "#" + d.data.id;
      });

      cell.select('text').remove();

      cell.append("text").attr("clip-path", function (d) {
        return "url(#clip-" + d.data.id + ")";
      }).selectAll("tspan").data(function (d) {
        return d.data.name.split(/(?=[A-Z][^A-Z])/g).concat([_this4.format(d.value)]);
      }).enter().append("tspan").attr("x", 4).attr("y", function (d, i) {
        return 13 + i * 10;
      }).text(function (d) {
        return d;
      });

      cell.select('title').remove();

      cell.append("title").text(function (d) {
        return d.data.id + "\n" + _this4.format(d.value);
      });

      allGroups.exit().remove();
    }
  }, {
    key: 'getSvgSize',
    value: function getSvgSize() {
      var height = 570;
      var containerWidth = this.getContainerWidth();

      return {
        width: containerWidth,
        height: height
      };
    }
  }, {
    key: 'getContainerWidth',
    value: function getContainerWidth() {
      return this.container.getBoundingClientRect().width;
    }
  }, {
    key: 'render',
    value: function render() {
      var _this5 = this;

      return _react2.default.createElement(
        'div',
        { className: 'tree-chart-container', ref: function ref(c) {
            _this5.container = c;
          } },
        _react2.default.createElement('svg', { ref: function ref(c) {
            _this5.svg = c;
          } }),
        _react2.default.createElement(_ChartLegend2.default, { data: this.state.lengendData })
      );
    }
  }]);

  return TreeChart;
}(_react2.default.Component);

TreeChart.propTypes = {
  config: _propTypes2.default.object.isRequired
};

exports.default = TreeChart;