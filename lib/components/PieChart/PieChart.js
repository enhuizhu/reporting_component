'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Chart2 = require('../../core/Chart');

var _Chart3 = _interopRequireDefault(_Chart2);

var _d = require('d3');

var d3 = _interopRequireWildcard(_d);

var _d3Tip = require('d3-tip');

var _d3Tip2 = _interopRequireDefault(_d3Tip);

var _ChartLegend = require('../ChartLegend/ChartLegend');

var _ChartLegend2 = _interopRequireDefault(_ChartLegend);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var PieChart = function (_Chart) {
  _inherits(PieChart, _Chart);

  function PieChart(props) {
    _classCallCheck(this, PieChart);

    var _this = _possibleConstructorReturn(this, (PieChart.__proto__ || Object.getPrototypeOf(PieChart)).call(this, props));

    _this.init();
    return _this;
  }

  _createClass(PieChart, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.drawChart();
      d3.select(this.svg).call(this.tooltip);
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps, prevState) {
      this.setTotalSize();
      this.drawChart();
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      window.removeEventListener('resize', this.drawChart);
    }
  }, {
    key: 'getLegendData',
    value: function getLegendData() {
      var _this2 = this;

      return this.props.config.data.map(function (v) {
        return {
          text: v.name,
          color: _this2.color(v.name)
        };
      });
    }
  }, {
    key: 'init',
    value: function init() {
      this.pie = d3.pie().sort(null).value(function (d) {
        return d.value;
      });

      this.padding = 10;
      this.setTotalSize();

      this.drawChart = this.drawChart.bind(this);
      this.getLabelText = this.getLabelText.bind(this);
      this.mouseover = this.mouseover.bind(this);
      this.mouseleave = this.mouseleave.bind(this);

      this.tooltip = (0, _d3Tip2.default)().attr("class", "d3-tip").offset([0, 0]).html(this.getLabelText);

      window.addEventListener('resize', this.drawChart);
    }
  }, {
    key: 'mouseover',
    value: function mouseover(d) {
      this.tooltip.show(d);
    }
  }, {
    key: 'mouseleave',
    value: function mouseleave() {
      this.tooltip.hide();
    }
  }, {
    key: 'setTotalSize',
    value: function setTotalSize() {
      this.totalSize = this.props.config.data.reduce(function (sum, current) {
        return sum += current.value;
      }, 0);

      console.log('totalSize', this.totalSize);
    }
  }, {
    key: 'getLabelText',
    value: function getLabelText(d) {
      return d.data.name + ': ' + this.formatValue(d.value) + ' ' + this.formatPercentage(d.value / this.totalSize);
    }
  }, {
    key: 'drawChart',
    value: function drawChart() {
      var _this3 = this;

      var that = this;
      this.size = this.getSvgSize();
      this.radius = Math.min(this.size.width, this.size.height) / 2;

      this.path = d3.arc().outerRadius(this.radius - this.padding).innerRadius(this.radius * 2 / 3);

      var arcTween = function arcTween(a) {
        var i = d3.interpolate(this._current, a);
        this._current = i(0);

        return function (t) {
          return that.path(i(t));
        };
      };

      d3.select(this.svg).select('g').remove();

      var arcs = d3.select(this.svg).attr('width', this.size.width).attr('height', this.size.height).append('g').on('mouseleave', this.mouseleave).attr('transform', 'translate(' + this.size.width / 2 + ', ' + this.size.height / 2 + ')').selectAll('.arc').data(this.pie(this.props.config.data));

      arcs.enter().append('path').on('mouseover', this.mouseover).attr('class', 'arc').each(function (d) {
        this._current = d;
      }).merge(arcs).transition().duration(1000).attrTween("d", arcTween).attr('fill', function (d, i) {
        return _this3.color(d.data.name);
      });

      arcs.exit().remove();
    }
  }, {
    key: 'render',
    value: function render() {
      var _this4 = this;

      return _react2.default.createElement(
        'div',
        { className: 'pie-chart-container', ref: function ref(c) {
            _this4.container = c;
          } },
        _react2.default.createElement('svg', { ref: function ref(c) {
            _this4.svg = c;
          } }),
        _react2.default.createElement(_ChartLegend2.default, {
          data: this.getLegendData()
        })
      );
    }
  }]);

  return PieChart;
}(_Chart3.default);

PieChart.propTypes = {
  config: _propTypes2.default.object.isRequired
};

exports.default = PieChart;