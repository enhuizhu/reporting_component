'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var PieChartDataProcesser = function () {
  function PieChartDataProcesser() {
    _classCallCheck(this, PieChartDataProcesser);
  }

  _createClass(PieChartDataProcesser, null, [{
    key: 'parse',
    value: function parse(data, target) {
      var newData = [];

      _lodash2.default.each(data, function (v, k) {
        newData.push({
          name: k,
          value: v[target].sum
        });
      });

      return newData;
    }
  }]);

  return PieChartDataProcesser;
}();

exports.default = PieChartDataProcesser;