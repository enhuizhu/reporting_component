'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _d = require('d3');

var d3 = _interopRequireWildcard(_d);

var _ChartLegend = require('../ChartLegend/ChartLegend');

var _ChartLegend2 = _interopRequireDefault(_ChartLegend);

var _Chart2 = require('../../core/Chart');

var _Chart3 = _interopRequireDefault(_Chart2);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               * ref: https://bl.ocks.org/mbostock/2e73ec84221cb9773f4c
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               **/


var PartitionChart = function (_Chart) {
  _inherits(PartitionChart, _Chart);

  function PartitionChart(props) {
    _classCallCheck(this, PartitionChart);

    var _this = _possibleConstructorReturn(this, (PartitionChart.__proto__ || Object.getPrototypeOf(PartitionChart)).call(this, props));

    _this.state = {
      legendsData: []
    };
    _this.resetChart = _this.resetChart.bind(_this);
    _this.prepareData();
    _this.init();
    return _this;
  }

  _createClass(PartitionChart, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.addResizeEvents();
      this.resetChart();
      this.setState({
        legendsData: this.getLengedsData()
      });
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps, prevState) {
      if (!_lodash2.default.isEqual(this.props.config.data, prevProps.config.data)) {
        this.prepareData();
        this.resetChart();
        this.setState({
          legendsData: this.getLengedsData()
        });
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      window.removeEventListener('resize', this.resetChart);
    }
  }, {
    key: 'prepareData',
    value: function prepareData() {
      this.root = d3.hierarchy(this.props.config.data).sum(function (d) {
        return d.size;
      }).sort(function (a, b) {
        return b.value - a.value;
      });
    }
  }, {
    key: 'getLengedsData',
    value: function getLengedsData() {
      var _this2 = this;

      if (!this.root.children) {
        return [];
      }

      var target = 1;

      var data = this.root.children.filter(function (v, i) {
        return v.depth === target;
      }).map(function (v) {
        return {
          color: _this2.color(v.data.name),
          text: v.data.name
        };
      });

      return data;
    }
  }, {
    key: 'init',
    value: function init() {
      this.color = d3.scaleOrdinal(d3.schemeCategory10);
    }
  }, {
    key: 'addResizeEvents',
    value: function addResizeEvents() {
      window.addEventListener('resize', this.resetChart);
    }
  }, {
    key: 'resetChart',
    value: function resetChart() {
      this.size = this.getSvgSize();
      this.x = d3.scaleLinear().range([0, this.size.width]).domain([0, this.size.width]);
      this.y = d3.scaleLinear().range([0, this.size.height]).domain([0, this.size.height]);

      d3.select(this.svg).attr('width', this.size.width).attr('height', this.size.height);

      this.partition = d3.partition().size([this.size.height, this.size.width]).padding(1).round(true);

      this.partition(this.root);

      this.draw();
    }
  }, {
    key: 'draw',
    value: function draw() {
      var _this3 = this;

      var nodes = d3.select(this.svg).selectAll(".node").data(this.root.descendants());

      var cell = nodes.enter().append("g").merge(nodes).attr("class", function (d) {
        return "node" + (d.children ? " node--internal" : " node--leaf");
      }).attr("transform", function (d) {
        return "translate(" + _this3.y(d.y0) + "," + _this3.x(d.x0) + ")";
      });

      cell.select('rect').remove();

      cell.append("rect").attr("id", function (d) {
        return "rect-" + d.data.name;
      }).attr("width", function (d) {
        return _this3.y(d.y1 - d.y0);
      }).attr("height", function (d) {
        return _this3.x(d.x1 - d.x0);
      })
      // .filter((d) => { 
      //   return !d.children; 
      // })
      .style("fill", function (d) {
        while (d.depth > 1) {
          d = d.parent;
        }return _this3.color(d.data.name);
      });

      cell.select('clipPath').remove();

      cell.append("clipPath").attr("id", function (d) {
        return "clip-" + d.data.name;
      }).append("use").attr("xlink:href", function (d) {
        return "#rect-" + d.data.name + "";
      });

      cell.select('text').remove();

      cell.append("text").attr("clip-path", function (d) {
        return "url(#clip-" + d.data.name + ")";
      }).attr("x", 4).selectAll("tspan").data(function (d) {
        return [d.data.name, " " + _this3.formatValue(d.value)];
      }).enter().append("tspan").attr("y", function (d, i) {
        return 13 * (i + 1);
      }).attr("x", 3).text(function (d) {
        return d;
      });

      cell.select('title').remove();

      cell.append("title").text(function (d) {
        return d.data.name + "\n" + _this3.formatValue(d.value);
      });

      nodes.exit().remove();
    }
  }, {
    key: 'render',
    value: function render() {
      var _this4 = this;

      return _react2.default.createElement(
        'div',
        { className: 'partition-chart-container', ref: function ref(c) {
            _this4.container = c;
          } },
        _react2.default.createElement('svg', { ref: function ref(c) {
            _this4.svg = c;
          } }),
        _react2.default.createElement(_ChartLegend2.default, {
          data: this.state.legendsData
        })
      );
    }
  }]);

  return PartitionChart;
}(_Chart3.default);

exports.default = PartitionChart;