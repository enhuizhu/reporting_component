'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _d = require('d3');

var d3 = _interopRequireWildcard(_d);

var _d3Sankey = require('d3-sankey');

var _Chart2 = require('../../core/Chart');

var _Chart3 = _interopRequireDefault(_Chart2);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// import sankey from './sankey';

var SankeyChart = function (_Chart) {
  _inherits(SankeyChart, _Chart);

  function SankeyChart(props) {
    _classCallCheck(this, SankeyChart);

    var _this = _possibleConstructorReturn(this, (SankeyChart.__proto__ || Object.getPrototypeOf(SankeyChart)).call(this, props));

    _this.init();
    return _this;
  }

  _createClass(SankeyChart, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.drawChart();
    }
  }, {
    key: 'init',
    value: function init() {
      this.margin = {
        top: 10,
        right: 10,
        bottom: 10,
        left: 10
      };

      this.color = d3.scaleOrdinal(d3.schemeCategory20);
      this.drawChart = this.drawChart.bind(this);

      window.addEventListener('resize', this.drawChart);
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      window.removeEventListener('resize', this.drawChart);
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps, prevState) {
      if (!_lodash2.default.isEqual(this.props.config.data, prevProps.config.data)) {
        this.drawChart();
      }
    }
  }, {
    key: 'setUpGraphData',
    value: function setUpGraphData() {
      var _this2 = this;

      this.graph = { 'nodes': [], 'links': [] };
      this.size = this.getSvgSize(1000);

      this.props.config.data.forEach(function (d) {
        if (d.value < 0) {
          console.log(d.value);
        }

        d.value = Math.abs(d.value);

        _this2.graph.nodes.push({ 'name': d.source });
        _this2.graph.nodes.push({ 'name': d.target });
        _this2.graph.links.push({
          'source': d.source,
          'target': d.target,
          'value': Math.abs(d.value)
        });
      });

      var nestData = d3.nest().key(function (d) {
        return d.name;
      }).map(this.graph.nodes);

      // return only the district / unique nodes
      this.graph.nodes = Object.keys(nestData).map(function (v) {
        return v.substr(1);
      });

      // loop through each link replacing the text with its index from node
      this.graph.links.forEach(function (d, i) {
        _this2.graph.links[i].source = _this2.graph.nodes.indexOf(_this2.graph.links[i].source);
        _this2.graph.links[i].target = _this2.graph.nodes.indexOf(_this2.graph.links[i].target);
      });

      //now loop through each nodes to make nodes an array of objects
      // rather than an array of strings
      this.graph.nodes.forEach(function (d, i) {
        _this2.graph.nodes[i] = { "name": d };
      });

      this.sankey = (0, _d3Sankey.sankey)().nodeWidth(5).nodePadding(40).extent([[1, 1], [this.size.width - this.margin.right - this.margin.left - 20, this.size.height - this.margin.top - this.margin.bottom]]);;

      this.sankey(this.graph);

      this.path = _d3Sankey.sankeyLinkHorizontal;
    }
  }, {
    key: 'drawChart',
    value: function drawChart() {
      this.setUpGraphData();

      d3.select(this.svg).select('g').remove();

      this.sankeyChart = d3.select(this.svg).attr('width', this.size.width).attr('height', this.size.height).append('g').attr('transform', 'translate(' + this.margin.left + ', ' + this.margin.top + ')');

      this.addLinks();
      this.addNodes();
    }
  }, {
    key: 'addLinks',
    value: function addLinks() {
      var _this3 = this;

      var link = this.sankeyChart.append("g").selectAll(".link").data(this.graph.links).enter().append("path").attr("class", "link").attr("d", function (d, i) {
        return (0, _d3Sankey.sankeyLinkHorizontal)()(d);
      }).style("stroke", function (d, i) {
        return _this3.color(d.target.name);
      }).style("stroke-width", function (d) {
        return Math.max(1, d.width);
      }).sort(function (a, b) {
        return b.dy - a.dy;
      });

      link.append("title").text(function (d) {
        return d.source.name + " → " + d.target.name + "\n" + _this3.formatValue(d.value);
      });
    }
  }, {
    key: 'addNodes',
    value: function addNodes() {
      var _this4 = this;

      var node = this.sankeyChart.append("g").selectAll(".node").data(this.graph.nodes).enter().append("g").attr("class", "node");

      // add the rectangles for the nodes
      node.append("rect").attr("x", function (d) {
        return d.x0;
      }).attr("y", function (d) {
        return d.y0;
      }).attr("height", function (d) {
        return Math.abs(d.y1 - d.y0);
      }).attr("width", function (d) {
        return Math.abs(d.x1 - d.x0);
      }).style("fill", function (d) {
        return d.color = _this4.color(d.name);
      }).style("stroke", function (d) {
        return d3.rgb(d.color).darker(2);
      }).append("title").text(function (d) {
        return d.name + "\n" + _this4.formatValue(d.value);
      });

      // add in the title for the nodes
      node.append("text").attr("x", function (d) {
        return d.x0 - 6;
      }).attr("y", function (d) {
        return (d.y1 + d.y0) / 2;
      }).attr("dy", "0.35em").attr("text-anchor", "end").text(function (d) {
        return d.name;
      }).filter(function (d) {
        return d.x0 < _this4.size.width / 2;
      }).attr("x", function (d) {
        return d.x1 + 6;
      }).attr("text-anchor", "start");
    }
  }, {
    key: 'render',
    value: function render() {
      var _this5 = this;

      return _react2.default.createElement(
        'div',
        { className: 'sankey-chart-container', ref: function ref(c) {
            _this5.container = c;
          } },
        _react2.default.createElement('svg', { ref: function ref(c) {
            _this5.svg = c;
          } })
      );
    }
  }]);

  return SankeyChart;
}(_Chart3.default);

exports.default = SankeyChart;