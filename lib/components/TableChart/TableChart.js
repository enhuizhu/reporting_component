'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _d = require('d3');

var d3 = _interopRequireWildcard(_d);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _Chart2 = require('../../core/Chart');

var _Chart3 = _interopRequireDefault(_Chart2);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var TableChart = function (_Chart) {
  _inherits(TableChart, _Chart);

  function TableChart(props) {
    _classCallCheck(this, TableChart);

    var _this = _possibleConstructorReturn(this, (TableChart.__proto__ || Object.getPrototypeOf(TableChart)).call(this, props));

    _this.setHeaderWidth = _this.setHeaderWidth.bind(_this);
    _this.prepareData(_this.props.config.data);
    _this.addResizeEvents();
    return _this;
  }

  _createClass(TableChart, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.setHeaderWidth();
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      window.removeEventListener('resize', this.setHeaderWidth);
    }
  }, {
    key: 'componentWillUpdate',
    value: function componentWillUpdate(nextProps, nextState) {
      this.prepareData(nextProps.config.data);
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps, prevState) {
      this.setHeaderWidth();
    }
  }, {
    key: 'addResizeEvents',
    value: function addResizeEvents() {
      window.addEventListener('resize', this.setHeaderWidth);
    }
  }, {
    key: 'setHeaderWidth',
    value: function setHeaderWidth() {
      if (!this.tbody) {
        return;
      }

      var tdWidths = [];

      if (this.tbody.children.length) {
        for (var i = 0; i < this.tbody.children[0].children.length; i++) {
          var item = this.tbody.children[0].children[i];
          this.header.children[i].style.width = item.getBoundingClientRect().width + 'px';
        }
      }
    }
  }, {
    key: 'prepareData',
    value: function prepareData(data) {
      this.root = d3.hierarchy(data).sum(function (d) {
        return d.size;
      }).sort(function (a, b) {
        return b.value - a.value;
      });

      this.treemap = d3.treemap().tile(d3.treemapResquarify).round(true).paddingInner(1);

      this.treemap(this.root);
    }
  }, {
    key: 'getTds',
    value: function getTds(v) {
      var tds = [];

      var name = v.data.name;
      var percentage = v.value / this.root.value;
      var value = v.value;

      while (v.parent) {
        if (v.parent.depth !== 0) {
          tds.push(_react2.default.createElement(
            'td',
            {
              key: _lodash2.default.uniqueId()
            },
            v.parent.data.name
          ));
        }

        v = v.parent;
      }

      tds.reverse();

      tds.push(_react2.default.createElement(
        'td',
        { key: _lodash2.default.uniqueId() },
        name
      ));

      for (var i = tds.length; i < this.root.height; i++) {
        tds.push(_react2.default.createElement('td', { key: _lodash2.default.uniqueId() }));
      }

      tds.push(_react2.default.createElement(
        'td',
        { key: _lodash2.default.uniqueId() },
        this.formatPercentage(percentage)
      ));
      tds.push(_react2.default.createElement(
        'td',
        { key: _lodash2.default.uniqueId() },
        this.formatValue(value)
      ));

      return tds;
    }
  }, {
    key: 'getChildren',
    value: function getChildren(child) {
      var _this2 = this;

      var trs = [];

      if (!child.children && !child.parent.render) {
        child.parent.children.map(function (v, i) {
          trs.push(_react2.default.createElement(
            'tr',
            { key: _lodash2.default.uniqueId() },
            _this2.getTds(v)
          ));
        });

        child.parent.render = true;
      } else if (child.children) {
        child.children.map(function (v) {
          trs = trs.concat(_this2.getChildren(v));
        });
      }

      return trs;
    }
  }, {
    key: 'render',
    value: function render() {
      var _this3 = this;

      if (!this.root.children) {
        return _react2.default.createElement(
          'div',
          null,
          ' No Data'
        );
      }

      var trs = this.getChildren(this.root);

      return _react2.default.createElement(
        'div',
        { className: 'table-chart-container' },
        _react2.default.createElement(
          'table',
          { className: 'table-header' },
          _react2.default.createElement(
            'thead',
            null,
            _react2.default.createElement(
              'tr',
              { ref: function ref(c) {
                  _this3.header = c;
                } },
              new Array(this.root.height + 2).fill('').map(function (v, i) {
                return _react2.default.createElement(
                  'td',
                  { key: i },
                  _this3.props.config.header[i]
                );
              })
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'table-wrapper' },
          _react2.default.createElement(
            'table',
            null,
            _react2.default.createElement(
              'tbody',
              { ref: function ref(c) {
                  _this3.tbody = c;
                } },
              trs
            ),
            _react2.default.createElement(
              'tfoot',
              null,
              _react2.default.createElement(
                'tr',
                null,
                _react2.default.createElement(
                  'td',
                  { colSpan: this.root.height + 2, className: 'total' },
                  'Total: ',
                  this.formatValue(this.root.value)
                )
              )
            )
          )
        )
      );
    }
  }]);

  return TableChart;
}(_Chart3.default);

TableChart.propTypes = {
  config: _propTypes2.default.object.isRequired
};

exports.default = TableChart;