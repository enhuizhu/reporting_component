'use strict';

import React from 'react';
import ReactDom from 'react-dom';
import ChartLegend from '../../src/components/ChartLegend/ChartLegend';
import * as d3 from "d3";

export class App extends React.Component {
  constructor(props) {
    super(props);

    this.data = [];

    this.color = d3.scaleOrdinal(d3.schemeCategory20c);

    for(let i = 0 ; i < 20; i++) {
      const text = 'test' + i;
      this.data.push({
        text,
        color: this.color(text),
      });
    }
  }

  onMouseOver(d, i) {
    console.log('d, i', d, i);
  }

  render() {
    return (
      <div>
        <h1>ChartLegend</h1>
        <ChartLegend data={this.data} mouseOver={this.onMouseOver.bind(this)}/>
      </div>
    );
  }
}

ReactDom.render(<App/>, document.getElementById('app'));