'use strict';

import React from 'react';
import ReactDom from 'react-dom';
import TableChart from '../../src/components/TableChart/TableChart';
import SunburstDataProcesser from '../../src/components/SunburstChart/SunburstDataProcesser';

export class App extends React.Component {
  constructor(props) {
    super(props);
    // this.state = {
    //   config: {
    //     data: require('../SunburstChart/flare.json'),
    //     header: ['header1', 'header2', 'header3', 'header4', 'percentage', 'total'],
    //   }
    // }

    this.state = {
      config : {
        data: {children:[],name:"Total"},
        header: ['header1', 'header2', 'header3', 'header4', 'percentage', 'total'],
      }
    }    
  }

   changeData() {
    this.data = require('../SunburstChart/sampleData.json');
    
    this.setState({
      config: {
        data: SunburstDataProcesser.parse(this.data, 'distributedCosts.amount'),
        header: ['header1', 'header2', 'header3', 'percentage', 'total'],
      }
    })
  }

  render() {
    return (
      <div>
        <h1>TableChart</h1>

        <button onClick={this.changeData.bind(this)}> change config </button>

        <TableChart config={this.state.config}/>
      </div>
    );
  }
}

ReactDom.render(<App/>, document.getElementById('app'));