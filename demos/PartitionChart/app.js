'use strict';

import React from 'react';
import ReactDom from 'react-dom';
import SunburstDataProcesser from '../../src/components/SunburstChart/SunburstDataProcesser';
import PartitionChart from '../../src/components/PartitionChart/PartitionChart';

export class App extends React.Component {
  constructor(props) {
    super(props);

    this.data = require('../SunburstChart/sampleData.json');
    
    // this.state = {
    //   config : {
    //     data: SunburstDataProcesser.parse(this.data, 'distributedCosts.amount')
    //   }
    // }
    this.state = {
      config : {
        data: {children:[],name:"Total"}
      }
    }    
  }

  changeData() {
    this.setState({
      config: {
        data: require('../SunburstChart/flare.json')
      }
    })
  }

  render() {
    return (
      <div>
        <h1>PartitionChart</h1>
        <button onClick={this.changeData.bind(this)}> change config </button>
        <PartitionChart config={this.state.config}/>
      </div>
    );
  }
}

ReactDom.render(<App/>, document.getElementById('app'));