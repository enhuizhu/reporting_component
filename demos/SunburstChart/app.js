'use strict';

import React from 'react';
import ReactDom from 'react-dom';
import SunburstChart from '../../src/components/SunburstChart/SunburstChart';
import SunburstDataProcesser from '../../src/components/SunburstChart/SunburstDataProcesser';

export class App extends React.Component {
  constructor(props) {
    super(props);
    this.data = require('./sampleData.json');
    // this.state = {
    //   config : {
    //     data: SunburstDataProcesser.parse(this.data, 'distributedCosts.amount')
    //   }
    // }

    this.state = {
      config : {
        data: {children:[],name:"Total"}
      }
    }
  }

  changeData() {
    this.setState({
      config: {
        data: require('./flare.json')
      }
    })
  }

  render() {
    return (
      <div>
        <h1>SunburstChart</h1>
        <button onClick={this.changeData.bind(this)}> change config </button>
        <div className='container'>
          <SunburstChart config={this.state.config}/>
        </div>
      </div>
    );
  }
}

ReactDom.render(<App/>, document.getElementById('app'));