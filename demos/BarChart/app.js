'use strict';

import React from 'react';
import ReactDom from 'react-dom';
import BarChart from '../../src/components/BarChart/BarChart';

export class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <h1>BarChart</h1>
        <BarChart/>
      </div>
    );
  }
}

ReactDom.render(<App/>, document.getElementById('app'));