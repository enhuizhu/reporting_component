'use strict';

import React from 'react';
import ReactDom from 'react-dom';
import ChordChart from '../../src/components/ChordChart/ChordChart';
import ChordDataProcesser from '../../src/components/ChordChart/ChordDataProcesser';

export class App extends React.Component {
  constructor(props) {
    super(props);
  
    const jsonData = require('./readme.json');

    this.config = {
      data: ChordDataProcesser.parse(jsonData, 'distributedCosts.amount'),
    }
  }

  changeData() {
    const jsonData2 = require('./sampleData.json');

    this.setState({
      config: {
        data: ChordDataProcesser.parse(jsonData2, 'distributedCosts.amount'),
      }
    })
  }

  render() {
    return (
      <div>
        <h1>ChordChart</h1>
        <button onClick={this.changeData.bind(this)}> change config </button>
        <ChordChart config={this.config}/>
      </div>
    );
  }
}

ReactDom.render(<App/>, document.getElementById('app'));