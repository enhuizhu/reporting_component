'use strict';

import React from 'react';
import ReactDom from 'react-dom';
import TreeMapChart from '../../src/components/TreeMapChart/TreeMapChart';
import SunburstDataProcesser from '../../src/components/SunburstChart/SunburstDataProcesser';

import * as d3 from "d3";

export class App extends React.Component {
  constructor(props) {
    super(props);
    
    this.data = require('./sampleData.json');
    
    // this.state = {
    //   config : {
    //     data: SunburstDataProcesser.parse(this.data, 'distributedCosts.amount')
    //   }
    // }

    this.state = {
      config : {
        data: {children:[],name:"Total"}
      }
    }    
  }

  changeData() {
    this.setState({
      config: {
        data: require('./flare.json')
      }
    })
  }

  render() {
    return (
      <div>
        <h1>TreeMapChart</h1>
        <button onClick={this.changeData.bind(this)}> change config </button>
        <TreeMapChart config={this.state.config}/>
      </div>
    );
  }
}

ReactDom.render(<App/>, document.getElementById('app'));