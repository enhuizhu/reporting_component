'use strict';

import React from 'react';
import ReactDom from 'react-dom';
import TreeChart from '../../src/components/TreeChart/TreeChart';
import SunburstDataProcesser from '../../src/components/SunburstChart/SunburstDataProcesser';

export class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: require('../SunburstChart/flare.json'),
    };
  }

  changeData() {
    const data = require('../SunburstChart/sampleData.json');
    
    this.setState({
      data: SunburstDataProcesser.parse(data, 'distributedCosts.amount')
    })
  }

  render() {
    return (
      <div>
        <h1>TreeChart</h1>
        <button onClick={this.changeData.bind(this)}> change config </button>
        <TreeChart config={{data: this.state.data}}/>
      </div>
    );
  }
}

ReactDom.render(<App/>, document.getElementById('app'));