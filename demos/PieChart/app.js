'use strict';

import React from 'react';
import ReactDom from 'react-dom';
import PieChart from '../../src/components/PieChart/PieChart';

export class App extends React.Component {
  constructor(props) {
    super(props);

    const data = [
      {name: 'Company A', value: 131},
      {name: 'Company B', value: 74},
      {name: 'Company C', value: 97}
    ];

    this.state = {
      config: {
        data,
      }
    }
  }

  changeData() {
     const data = [
      {name: 'Company A', value: 131},
      {name: 'Company B', value: 74},
      {name: 'Company C', value: 97},
      {name: 'Company D', value: 97},
    ];

    this.setState({
      config: {
        data,
      }
    });
  }

  render() {
    return (
      <div>
        <h1>PieChart</h1>
        <button onClick={this.changeData.bind(this)}> change config </button>
        <PieChart config={this.state.config}/>
      </div>
    );
  }
}

ReactDom.render(<App/>, document.getElementById('app'));