'use strict';

import React from 'react';
import ReactDom from 'react-dom';
import SankeyChart from '../../src/components/SankeyChart/SankeyChart';
import ChordDataProcesser from '../../src/components/ChordChart/ChordDataProcesser';
import * as d3 from 'd3';

export class App extends React.Component {
  constructor(props) {
    super(props);

    const jsonData = require('./sampleData.json');
    const chartData = ChordDataProcesser.parse(jsonData, 'distributedCosts.amount');
    let data = [ 
        { source: '410_00', target: '331_00', value: 808319.622360212 },
        { source: '410_00', target: '765_00', value: 236519.63228711413 },
        { source: '410_00', target: '502_00', value: 328801.7645839938 },
        { source: '410_00', target: '761_00', value: 8767345.53500877 }, 
    ];
    this.state = {
      chartConfig: {
        data: data,
      }
    }
  }

  changeData() {
    const jsonData = require('./sampleData2.json');
    const chartData = ChordDataProcesser.parse(jsonData, 'distributedCosts.amount');
    
    console.log('chartData', chartData);
    let data = [ 
        { source: '410_00', target: '331_00', value: 808319.622360212 },
        { source: '410_00', target: '765_00', value: 236519.63228711413 },
        { source: '410_00', target: '502_00', value: 328801.7645839938 },
        { source: '410_00', target: '761_00', value: 8767345.53500877 }, 
    ];
    
    this.setState({
      chartConfig: {
        data: data,
      }
    })
  }

  render() {
    return (
      <div>
        <h1>SankeyChart</h1>
        <button onClick={this.changeData.bind(this)}> change config </button>
        <SankeyChart config={{data: this.state.chartConfig.data}}/> :
      </div>
    );
  }
}

ReactDom.render(<App/>, document.getElementById('app'));