import { expect } from 'chai';
import _ from 'lodash';
import SunburstDataProcesser from '../../../src/components/SunburstChart/SunburstDataProcesser';

describe('SunburstDataProcesser', () => {
  const target = 'distributedCosts.amount';

  it('parse', () => {
    const data = {
      "Databases": {
        "DBMS": {
          "Sybase LSEG (non LCH)": {
            "distributedCosts.amount": {
              "count": 680,
              "sum": 1459515.803007821,
              "avg": 2146.3467691291485,
              "min": -18001.14475336464,
              "max": 61043.32195023976
            }
          },
        },
        "DB Utilities & Data Access Tools": {
          "distributedCosts.amount": {
            "count": 2829588,
            "sum": 510722.9498855093,
            "avg": 0.18049375028644077,
            "min": -217.7583823314093,
            "max": 48278.99081347278
          }
        },
      }
    };
    

    const expectedResult = {
      name: 'Total',
      children: [{
        name: 'Databases',
        children: [
          {
            name: 'DBMS',
            children: [{
              name: 'Sybase LSEG (non LCH)',
              size: 1459515.803007821,
            }]
          },
          {
            name: 'DB Utilities & Data Access Tools',
            size: 510722.9498855093,
          },
        ]
      }]
    }

    expect(SunburstDataProcesser.parse(data, target)).to.deep.equal(expectedResult);
  });


  it('parse, when child only have one level', () => {
    const data = {
      "DBMS": {
        "distributedCosts.amount": {
          "count": 680,
          "sum": 1459515.803007821,
          "avg": 2146.3467691291485,
          "min": -18001.14475336464,
          "max": 61043.32195023976
        }
      },
      "DB Utilities & Data Access Tools": {
        "distributedCosts.amount": {
          "count": 2829588,
          "sum": 510722.9498855093,
          "avg": 0.18049375028644077,
          "min": -217.7583823314093,
          "max": 48278.99081347278
        }
      },
    };

    const expectedResult = {
      name: 'Total',
      children: [
        {
          name: 'DBMS',
          size: 1459515.803007821,
        },
        {
          name: 'DB Utilities & Data Access Tools',
          size: 510722.9498855093,
        },
      ]
    };

    const parsedResult = SunburstDataProcesser.parse(data, target);
    expect(parsedResult).to.deep.equal(expectedResult);
  });


  it('getChildren', () => {
    const childrenObj = {
      "DBMS": {
        "Sybase LSEG (non LCH)": {
          "distributedCosts.amount": {
            "count": 680,
            "sum": 1459515.803007821,
            "avg": 2146.3467691291485,
            "min": -18001.14475336464,
            "max": 61043.32195023976
          }
        },
      },
      "DB Utilities & Data Access Tools": {
        "distributedCosts.amount": {
          "count": 2829588,
          "sum": 510722.9498855093,
          "avg": 0.18049375028644077,
          "min": -217.7583823314093,
          "max": 48278.99081347278
        }
      },
    };

    const expectedResult = [
      {
        name: 'DBMS',
        children: [{
          name: 'Sybase LSEG (non LCH)',
          size: 1459515.803007821,
        }]
      },
      {
        name: 'DB Utilities & Data Access Tools',
        size: 510722.9498855093,
      },
    ];

    const parsedResult = SunburstDataProcesser.getChildren(childrenObj, 'distributedCosts.amount');

    expect(parsedResult).to.deep.equal(expectedResult);
  });
});