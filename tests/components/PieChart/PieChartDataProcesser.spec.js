import { expect } from 'chai';
import _ from 'lodash';
import PieChartDataProcesser from '../../../src/components/PieChart/PieChartDataProcesser';

describe('PieChartDataProcesser', () => {
  it('parse', () => {
    let testData = {
      "Staff": {
        "amount": {
          "count": 13175,
          "sum": 2.138774418712374E7,
          "avg": 1623.3581925710616,
          "min": -18320.46017546565,
          "max": 499055.6828792124
        }
      },
      "Non-Staff": {
        "amount": {
          "count": 4725,
          "sum": 1.2746770106802193E7,
          "avg": 2697.7291231327395,
          "min": 0.0,
          "max": 2108061.034761874
        }
      },
      "Other": {
        "amount": {
          "count": 26,
          "sum": 4743062.434782956,
          "avg": 182425.47826088293,
          "min": 1589.08324144908,
          "max": 1394683.23172925
        }
      }
    };

    let expectData = [
      { name: 'Staff', value: 2.138774418712374E7 },
      { name: 'Non-Staff', value: 1.2746770106802193E7 },
      { name: 'Other', value: 4743062.434782956 },
    ];

    const result = PieChartDataProcesser.parse(testData, "amount");
    expect(result).to.deep.equal(expectData);
  });
});
