require('../../TestHelper');

import React from 'react';
import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import sinon from 'sinon';
import * as d3 from 'd3';
import ChordChart from '../../../src/components/ChordChart/ChordChart';

describe('<ChordChart />', () => {
  let wrapper, instance;
  let config = {
    data: [ 
      { source: '331_00', target: '331_00', value: 18453522.989087842 },
      { source: '765_00', target: '765_00', value: 112640 } 
    ]
  };

  beforeEach(() => {
    ChordChart.prototype.container = {
      getBoundingClientRect: () => {
        return {
          width: 800,
          height: 800,
        }
      }
    };
    wrapper = shallow(<ChordChart config={config}/>);
    instance = wrapper.instance();
  });

  
  it('getChrodData', () => {
    instance.indexByName = d3.map();
    instance.nameByIndex = d3.map();

    
    let results = instance.getChrodData(config.data);
    expect(results).to.deep.equal([ [ 18453522.989087842, 0 ], [ 0, 112640 ] ]);
  });

  it('getChrodData when data has more children', () => {
    instance.indexByName = d3.map();
    instance.nameByIndex = d3.map();

    let data = [ 
        { source: '410_00', target: '331_00', value: 808319.622360212 },
        { source: '410_00', target: '765_00', value: 236519.63228711413 },
        { source: '410_00', target: '502_00', value: 328801.7645839938 },
        { source: '410_00', target: '761_00', value: 8767345.53500877 }, 
    ];

    let results = instance.getChrodData(data);

    expect(results).to.deep.equal(
      [ 
        [ 
          808319.622360212,
          236519.63228711413,
          328801.7645839938,
          8767345.53500877 
        ] 
      ]
    );
  });
});