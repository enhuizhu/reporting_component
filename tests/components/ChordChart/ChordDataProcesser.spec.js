import { expect } from 'chai';
import _ from 'lodash';
import ChordDataProcesser from '../../../src/components/ChordChart/ChordDataProcesser';

describe('ChordDataProcesser', () => {
  it('parse', () => {
    let testData = {
      "331_00": {
        "331_00": {
          "distributedCosts.amount": {
            "count": 4,
            "sum": 1.8453522989087842E7,
            "avg": 4613380.747271961,
            "min": 904222.6264653042,
            "max": 7933169.533008862
          }
        }
      },
      "765_00": {
        "765_00": {
          "distributedCosts.amount": {
            "count": 1,
            "sum": 112640.0,
            "avg": 112640.0,
            "min": 112640.0,
            "max": 112640.0
          }
        }
      }
    };

    let expectData = [ 
      { source: '331_00', target: '331_00', value: 18453522.989087842 },
      { source: '765_00', target: '765_00', value: 112640 } 
    ];

    const result = ChordDataProcesser.parse(testData, "distributedCosts.amount");
    expect(result).to.deep.equal(expectData);
  });

  it('parse data have more children', () => {
    let data = {
      "410_00": {
        "331_00": {
          "distributedCosts.amount": {
            "count": 45616339,
            "sum": 808319.622360212,
            "avg": 0.017719958244790578,
            "min": -1876.0773183525075,
            "max": 8251.274390217355
          }
        },
        "765_00": {
          "distributedCosts.amount": {
            "count": 397300,
            "sum": 236519.63228711413,
            "avg": 0.5953174736650242,
            "min": -1728.211805774261,
            "max": 62542.58586986875
          }
        },
        "502_00": {
          "distributedCosts.amount": {
            "count": 508710,
            "sum": 328801.7645839938,
            "avg": 0.6463442129779124,
            "min": -10140.35081811148,
            "max": 28481.51845228863
          }
        },
        "761_00": {
          "distributedCosts.amount": {
            "count": 5116623,
            "sum": 8767345.53500877,
            "avg": 1.7135023500869166,
            "min": -16957.2837009155,
            "max": 2397756.0
          }
        }
      }
    };

    const result = ChordDataProcesser.parse(data, "distributedCosts.amount");
    expect(result).to.deep.equal(
      [ 
        { source: '410_00', target: '331_00', value: 808319.622360212 },
        { source: '410_00', target: '765_00', value: 236519.63228711413 },
        { source: '410_00', target: '502_00', value: 328801.7645839938 },
        { source: '410_00', target: '761_00', value: 8767345.53500877 }, 
      ]
    );
  });


});
